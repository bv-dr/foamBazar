**Warning** This document is not up to date with the most recent version of foamStar. It is kept here for records only.

For a user guide, one should refer to the wiki associated to the foamStar repository.