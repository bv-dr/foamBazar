# Python scripts

This folder contains useful python scripts for pre and post processes of foamStar cases. 

These are no longer updated or supported, as most should eventually be merged into [ideFoam Python Package](https://gitlab.com/bvecn/ide-foam). 
