/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      fvSolution;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //


solvers
{
    "alpha.water.*"
    {
        nAlphaCorr      2;
        nAlphaSubCycles 1;
        cAlpha          1;
        icAlpha         0;

        MULESCorr       yes;
        nLimiterIter    10;
        alphaApplyPrevCorr  no;

        solver          smoothSolver;
        smoother        symGaussSeidel;
        tolerance       1e-10;
        relTol          0;
        minIter         2;
	cCompress	0;
    }

    "pcorr.*"
    {
        solver          GAMG;
        smoother        DIC;
        agglomerator    faceAreaPair;
        mergeLevels     1;
        nCellsInCoarsestLevel 10;
        cacheAgglomeration true;

        tolerance       1e-4;
        relTol          0;
    };

    p_rgh
    {
        solver          GAMG;
        smoother        DIC;
        agglomerator    faceAreaPair;
        mergeLevels     1;
        nCellsInCoarsestLevel 10;
        cacheAgglomeration true;

        tolerance       1e-9;
        relTol          1e-2;
    };

    p_rghFinal
    {
        $p_rgh;
        relTol          1e-3;
    }

    "(U|k|omega).*"
    {
	//solver		smoothSolver;
	//smoother	symGaussSeidel;
        
	solver          PBiCGStab;
	preconditioner DILU;
        
	nSweeps         1;

        tolerance       1e-8;
        relTol          0;
        minIter         1;
    };

    "cellDisplacement.*"
    {
        solver          GAMG;
        tolerance       1e-7;
        relTol          0;
        smoother        GaussSeidel;
        cacheAgglomeration true;
        nCellsInCoarsestLevel 10;
        agglomerator    faceAreaPair;
        mergeLevels     1;
    }
}

PIMPLE
{
    momentumPredictor   yes;
    fsiTol              1e-3;
    fsiMaxIter          21;
    nOuterCorrectors    6;
    nCorrectors         4;
    nNonOrthogonalCorrectors 1;
    correctPhi          no;
    moveMeshOuterCorrectors no;
}

relaxationFactors
{
    fields
    {
	//p_rgh 0.5;
        ".*" 0.5;
    }
    equations
    {
	//p_rgh 0.5;
        ".*" 0.5; 
    }
}

cache
{
    grad(U);
}


// ************************************************************************* //
