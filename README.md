# FoamBazar


This repository aims at centralizing documents about foamStar useful for users and developers. 

Some of these resources are no longer up-to-date, or clearly deprecated. Most of the time, this will be indicated in README.md inside the proper folders. However, users are advised to treat documents here with precautions. 

Associated repositories are: 

- foamStar: https://gitlab.com/bvecn/foamstar (with an associated wiki containing the most up-to-date documentation)
- ideFoam: https://gitlab.com/bvecn/ide-foam which is a python tool for pre- and postprocessing of foamStar runs. It contains the most up-to-date python scripts.


