#!/usr/bin/env python
###
###     Class designed to read variable format for given file path
###
###     Author  : YoungMyung Choi, Ecole Centrale de Nantes
###     mail to : youngmyung.choi@ec-nantes.fr
###
###     Date    : 24th Feb. 2017
###
import numpy as np

class varData:
    ### Initialize VarData with read file ----------------
    def __init__(self, filePath) :
        self._variables_= []
        self._rowData_= []
        self._colData_= []
        self._nVar_ = -1

        try :
            fid = open(filePath,'r')
        except "noFileError" as e :
            print(str(e))
            fid.close()
        else:
            flines = fid.readlines()
            for line in flines:
                tline = line.strip()
                if (tline[0] != "#") :
                    if tline[0:3] == "var" :
                        self._variables_ = tline.split()[1:]
                    else :
                        linesplitted = list(map(float, tline.split()))
                        self._rowData_.append(linesplitted)
            self._nVar_=len(self._rowData_[0])
            self._colData_=np.column_stack( self._rowData_ )
            fid.close()
    ### ---------------------------------------------------

    ### return number of variables
    def nVariables(self):
        return self._nVar_

    ### return variables
    def variables(self):
        return self._variables_

    ### return column Data
    def colData(self, * argList):
        if (len(argList) == 0):
            return self._colData_
        elif (len(argList) == 1):
            colIndex = argList[0]
            return self._colData_[colIndex][:]

    ### return row Data
    def rowData(self, * argList):
        if (len(argList) == 0):
            return self._rowData_
        elif (len(argList) == 1):
            rowIndex = argList[0]
            return self._rowData_[rowIndex][:]

    ### return data of given varName
    def varData(self, varName):
        try :
            varIndex=self._variables_.index(varName)
        except "noIndexError" as e :
            print(str(e))
            return 0
        else:
            return self._colData_[varIndex][:]
