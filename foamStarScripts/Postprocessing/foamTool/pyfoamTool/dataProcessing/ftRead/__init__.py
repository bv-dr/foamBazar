#!/usr/bin/env python
###
###     Class Package designed to read data for given file path
###
###     Author  : YoungMyung Choi, Ecole Centrale de Nantes
###     mail to : youngmyung.choi@ec-nantes.fr
###
###     Date    : 25th Feb. 2017
###

### variable data format Class
from .varData import varData

### wave gauge data format for foamStar
from .waveGauge import waveGauge
