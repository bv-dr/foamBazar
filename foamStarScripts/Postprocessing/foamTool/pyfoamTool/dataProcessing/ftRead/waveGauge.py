#!/usr/bin/env python
###
###     Class designed to read variable format for given file path
###
###     Author  : YoungMyung Choi, Ecole Centrale de Nantes
###     mail to : youngmyung.choi@ec-nantes.fr
###
###     Date    : 24th Feb. 2017
###
import numpy as np

class waveGauge:
    def __init__(self, filePath) :
        self.gaugeNames_ = []
        self.nGauge_     = []
        self._xPosition  = []
        self._yPosition  = []
        self._zPosition  = []
        self._position   = []
        self._rawData    = []

        try :
            fid = open(filePath,'r')
        except "noFileError" as e :
            print(str(e))
            self.variables={}
            self.rawData={}
            self.nVar = -1
            fid.close()
        else:
            tmpData =[]
            flines = fid.readlines()
            i=0
            tmpdata=[]
            for line in flines :
                i+=1
                tline = line.strip()
                if (i==2) :
                    self.gaugeNames_ = tline.split()[2:]
                    self.nGauge_=len(self.gaugeNames_)
                if (i==3) :
                    tmpXPosition = tline.split()[2:]
                if (i==4) :
                    tmpYPosition = tline.split()[2:]
                if (i==5) :
                    tmpZPosition = tline.split()[2:]
                if (i>5) :
                    linesplitted = map(float, tline.split())
                    tmpData.append(linesplitted)

            self._xPosition = map(float, tmpXPosition)
            self._yPosition = map(float, tmpYPosition)
            self._zPosition = map(float, tmpZPosition)

            tempPosition = [self._xPosition,
                            self._yPosition,
                            self._zPosition]

            self._position =np.column_stack(tempPosition)

            self._rawData=np.column_stack( tmpData )

            fid.close()

    def nGauge(self):
        return self.nGauge_

    def gaugeNames(self):
        return self.gaugeNames_

    def position(self, gaugeIndex):
        if (gaugeIndex >=0 and gaugeIndex < self.nGauge_):
            return self._position[gaugeIndex][:]

    def xPosition(self, gaugeIndex):
        if (gaugeIndex >=0 and gaugeIndex < self.nGauge_):
            return self._xPosition[gaugeIndex]

    def yPosition(self, gaugeIndex):
        if (gaugeIndex >=0 and gaugeIndex < self.nGauge_):
            return self._yPosition[gaugeIndex]

    def zPosition(self, gaugeIndex):
        if (gaugeIndex >=0 and gaugeIndex < self.nGauge_):
            return self._zPosition[gaugeIndex]

    def rawData(self):
        return self._rawData

    def time(self):
        return self._rawData[0][:]

    def gaugeData(self, gaugeIndex):
        if (gaugeIndex >=0 and gaugeIndex < self.nGauge_):
            return self._rawData[gaugeIndex+1][:]

### ---------------------------------------------------

