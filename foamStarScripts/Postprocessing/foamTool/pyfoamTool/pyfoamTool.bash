#!/bin/

### Set Python Package for foam Bazar
export ftDataProCessing=$pyFOAMTOOL/"dataProcessing"
export ftPostProCessing=$pyFOAMTOOL/"postProcessing"

### Add user defined Python Path
export PYTHONPATH=$PYTHONPATH:$ftDataProCessing
export PYTHONPATH=$PYTHONPATH:$ftPostProCessing

### Load fbPlot shell script
###
###   it defines the shell functions to plot
###
source $ftPostProCessing/"ftPlot.bash"
