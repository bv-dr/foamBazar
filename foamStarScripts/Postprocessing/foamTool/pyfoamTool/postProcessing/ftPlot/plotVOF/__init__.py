#!/usr/bin/env python
###
###     ecfoamTool
###
###      Python Package : "plotVOF"
###
###         it includes the modules to plot Volume of Fraction of OpenFOAM log
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 27th Feb. 2017
###

import plotVOFAll
import plotVOFSimple
