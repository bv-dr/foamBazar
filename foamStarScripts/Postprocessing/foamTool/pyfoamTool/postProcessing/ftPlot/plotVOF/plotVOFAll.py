#!/usr/bin/env python
###
###     ecfoamTool
###
###     Python Script : plotMassError.py
###
###         Python function to plot Continuity Error
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 27th Feb. 2017
###

### Python Module
import matplotlib.pyplot as plt
import numpy as np

### User Defined Module
import ftRead

### function to plot volume of fraction for given courant number file path
def fbplotVolumeOfFraction(logDirPath) :

    ### extracted VOF result file name from foamStar log extractor
    VOFMaxFileName = "/VOF.max"
    VOFMinFileName = "/VOF.min"
    VOFFileName    = "/VOF.vof"

    ### set file path
    VOFMaxFilePath = logDirPath
    VOFMaxFilePath+= VOFMaxFileName

    VOFMinFilePath = logDirPath
    VOFMinFilePath+= VOFMinFileName

    VOFFilePath    = logDirPath
    VOFFilePath   += VOFFileName

    ### load file
    VOFMaxFile = ftRead.varData(VOFMaxFilePath)
    VOFMinFile = ftRead.varData(VOFMinFilePath)
    VOFFile    = ftRead.varData(VOFFilePath)

    ### set variables to plot
    simulTime = VOFFile.colData(0)
    nPIMPLE   = VOFFile.colData(1)

    VOFMaxData = VOFMaxFile.rowData()
    VOFMinData = VOFMinFile.rowData()
    VOFData = VOFFile.rowData()

    ### set temporal variables to plot (OuterLoop)
    plotX        = []
    plotVOFMaxY  = []
    plotVOFMinY  = []
    plotVOFY     = []

    plotVOFMaxY1 = VOFMaxFile.colData(2)
    plotVOFMinY1 = VOFMinFile.colData(2)
    plotVOFY1 = VOFFile.colData(2)

    nTimeStep = len(simulTime)
    for it in range(0, nTimeStep-1):
        nIter  = int(nPIMPLE[it])

        deltaT = (simulTime[it + 1] - simulTime[it]) / (nIter-1)
        curTime=simulTime[it]

        for ii in range(0, nIter):
            plotX.append(curTime)

            plotVOFMaxY.append(VOFMaxData[it][2+ii])
            plotVOFMinY.append(VOFMinData[it][2+ii])
            plotVOFY.append(VOFData[it][2+ii])

            curTime = curTime + deltaT


    ### Plot Setting
    mkSize  = 8
    fntSize = 18
    lnWidth = 2

    ### set figure default setting
    fig = plt.figure(num = None, figsize=(10, 8), facecolor='w', edgecolor='k')

    ### set title
    titleName ="VOF All Result Plot"
    plt.title(titleName)

    ### plot volume of fraction result for simulation time and outer loop
    plt.plot(plotX, plotVOFMaxY,'g-',markersize=mkSize,linewidth=lnWidth)
    plt.plot(plotX, plotVOFMinY,'m-',markersize=mkSize,linewidth=lnWidth)
    plt.plot(plotX, plotVOFY,'k-',markersize=mkSize,linewidth=lnWidth)

    plt.plot(simulTime, plotVOFMaxY1,'ro',markersize=mkSize,linewidth=lnWidth)
    plt.plot(simulTime, plotVOFMinY1,'ro',markersize=mkSize,linewidth=lnWidth)
    plt.plot(simulTime, plotVOFY1,'ro',markersize=mkSize,linewidth=lnWidth)

    ### set axis label
    plt.xlabel('Time [s]',fontsize=fntSize)
    plt.ylabel('Volume of Fraction',fontsize=fntSize)

    ### set legend
    plt.legend(["min[alpha]","max[alpha]","Volume of Fraction"],
                numpoints=1,
                bbox_to_anchor=(0.5,0.995),
                loc=9,
                ncol=3,  borderaxespad=0.)

    ### set y limit
    plt.ylim([-0.05, 1.2])

    ### set grid
    plt.grid(b=True, which = 'major' , color = 'k', linestyle='-')
    plt.grid(b=True, which = 'minor' , color = 'k', linestyle='--')

    ### figure layout Setting
    plt.tight_layout()

    ### Show the plot
    plt.show()
