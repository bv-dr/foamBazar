#!/usr/bin/env python
###
###     foamBazar
###
###     Python Main Program : ecfoamTool
###
###         It plots volume of fraction of OpenFOAM log.
###
###     To run :
###
###         (1)  python plotVOF (default : /logResult)
###
###         (2)  python plotVOF "LogResultFolderPath"
###
###         Be careful, the input path of this function is directory
###         which includes massError.*
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 27th Feb. 2017
###

### Python Module
import os
import sys

### User Defined Module
import plotVOFAll
import plotVOFSimple

### Main Program
if __name__ == "__main__" :

    ### Test input argument and set file path from argument and option to plot
    casePath = os.getcwd()
    Narg = len(sys.argv)

    isSimplePlot = True
    isAllPlot = False

    logFolderPath = ""
    variableName  = ""

    argActive = [0] * Narg
    argActive[0] = 1

    for ii in range (0, Narg):

        if (sys.argv[ii] == "-a" or sys.argv[ii] == "-A" or
            sys.argv[ii] == "-all" or sys.argv[ii] == "-All"):

            isSimplePlot = False
            isAllPlot = True
            argActive[ii] = 1

        if (sys.argv[ii] == "-d" or sys.argv[ii] == "-D" or
            sys.argv[ii] == "-dir" or sys.argv[ii] == "-Dir" ):

            argActive[ii]   = 1
            argActive[ii+1] = 1

            logFolderPath  = sys.argv[ii+1]

            if (logFolderPath.endswith('/')):
                logFolderPath=logFolderPath[:-1]

            if (not logFolderPath.startswith('/')):
                logFolderPath="/"+logFolderPath

    if (logFolderPath == "") :
        logFolderPath = "/logResult"

    ### Input file path and variable name
    logDirPath  = casePath
    logDirPath += logFolderPath

    ### simple plot (VOF results with respect to only simulation time)
    if (isSimplePlot):
        plotVOFSimple.fbplotVolumeOfFractionSimple(logDirPath)

    ### detail plot (VOF results with respect to simulation time and outer loop)
    if (isAllPlot):
        plotVOFAll.fbplotVolumeOfFraction(logDirPath)
