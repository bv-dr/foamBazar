#!/usr/bin/env python
###
###     ecfoamTool
###
###     Python Script : plotMassError.py
###
###         Python function to plot Continuity Error
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 27th Feb. 2017
###

### Python Module
import matplotlib.pyplot as plt
import numpy as np

### User Defined Module
import ftRead

### function to plot volume of fraction for given courant number file path
def fbplotVolumeOfFractionSimple(logDirPath) :

    ### extracted VOF result file name from foamStar log extractor
    VOFSimpleFileName = "/VOF.simple"

    ### set file path
    VOFSimpleFilePath = logDirPath
    VOFSimpleFilePath+= VOFSimpleFileName

    ### load file
    VOFSimpleFile = ftRead.varData(VOFSimpleFilePath)

    ### set variables to plot
    simulTime = VOFSimpleFile.colData(0)
    nIter     = VOFSimpleFile.colData(1)
    VOFMin    = VOFSimpleFile.colData(2)
    VOFMax    = VOFSimpleFile.colData(3)
    VOF       = VOFSimpleFile.colData(4)

    ### Plot Setting
    mkSize  = 7
    fntSize = 18
    lnWidth = 2

    ### set figure default setting
    fig = plt.figure(num = None, figsize=(8, 6), facecolor='w', edgecolor='k')

    ### set title
    titleName ="VOF Simple Result Plot"
    plt.title(titleName)

    ### plot volume of fraction result for simulation time
    plt.plot(simulTime, VOFMin,'b-',markersize=mkSize,linewidth=lnWidth)
    plt.plot(simulTime, VOFMax,'r-',markersize=mkSize,linewidth=lnWidth)
    plt.plot(simulTime, VOF,'g-',markersize=mkSize,linewidth=lnWidth)

    ### set axis label
    plt.xlabel('Time [s]',fontsize=fntSize)
    plt.ylabel('Volume of Fraction',fontsize=fntSize)

    ### set legend
    plt.legend(["min[alpha]","max[alpha]","Volume of Fraction"],
                numpoints=1,
                bbox_to_anchor=(0.5,0.995),
                loc=9,
                ncol=3,  borderaxespad=0.)

    ### set y limit
    plt.ylim([-0.05, 1.2])

    ### set grid
    plt.grid(b=True, which = 'major' , color = 'k', linestyle='-')
    plt.grid(b=True, which = 'minor' , color = 'k', linestyle='--')

    ### figure layout Setting
    plt.tight_layout()

    ### Show the plot
    plt.show()
