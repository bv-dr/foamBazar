#!/usr/bin/env python
###
###     ecfoamTool
###
###     Python Script : plotResidualSimple.py
###
###         Python function to plot Residual for Outer Loop
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 27th Feb. 2017
###

### Python Module
import matplotlib.pyplot as plt
import numpy as np

### User Defined Module
import ftRead

### function to plot residual for given log directory
def fbplotResidualSimple(logFolderPath, variableName) :

    ### extracted residual file name from foamStar log extractor
    initialResidualFileName ="/initialResidual.0."
    finalResidualFileName   ="/finalResidual.0."
    nIterResidualFileName   ="/nIteration.0."

    initialResidualFileName +=variableName
    finalResidualFileName   +=variableName
    nIterResidualFileName   +=variableName

    ### set file path
    initialResidualPath = logFolderPath
    initialResidualPath+=initialResidualFileName

    finalResidualPath = logFolderPath
    finalResidualPath+=finalResidualFileName

    nIterResidualPath = logFolderPath
    nIterResidualPath+=nIterResidualFileName

    ### load file
    initialResidual = ftRead.varData(initialResidualPath)
    finalResidual   = ftRead.varData(finalResidualPath)
    nIteration   = ftRead.varData(nIterResidualPath)

    ### set variables to plot
    maxIter   = initialResidual.nVariables() - 2
    simulTime = initialResidual.colData(0)
    nPIMPLE   = initialResidual.colData(1)

    iniResData = initialResidual.rowData()
    finResData = finalResidual.rowData()
    nSolver   = nIteration.rowData()

    ### set temporal variables to plot (OuterLoop)
    plotX        = []
    plotIniResY  = []
    plotFinResY  = []

    plotnSolverY  = []

    plotiniResY1 = initialResidual.colData(2)
    plotfinResY1 = finalResidual.colData(2)
    plotSolY1 = nIteration.colData(2)

    nTimeStep = len(simulTime)
    for it in range(0, nTimeStep-1):
        nIter  = int(nPIMPLE[it])

        deltaT = (simulTime[it + 1] - simulTime[it]) / (nIter-1)
        curTime=simulTime[it]

        for ii in range(0, nIter):
            plotX.append(curTime)
            plotIniResY.append(iniResData[it][2+ii])
            plotFinResY.append(finResData[it][2+ii])
            plotnSolverY.append(nSolver[it][2+ii])
            curTime = curTime + deltaT

    ### Plot Setting
    mkSize  = 9
    fntSize = 18
    lnWidth = 2

    ### set figure default setting
    fig, axArr = plt.subplots(3, sharex = True , figsize = (12,10), facecolor ='w', edgecolor='k')

    ### set title
    titleName = "Residual :"
    titleName +=variableName
    axArr[0].set_title(titleName)


    ### plot initial residual for all outer loop and and denotes first and last step of outer loop
    axArr[0].plot(plotX, plotIniResY,'b',linewidth=lnWidth)
    axArr[0].plot(simulTime, plotiniResY1,'ro',markersize=mkSize,linewidth=lnWidth)

    ### set y log scale plot
    axArr[0].set_yscale('log')

    ### set axis label
    axArr[0].set_ylabel('Initial Res.',fontsize=fntSize)

    ### set legend
    # axArr[0].legend(["Initial Residual","Time Step"],
    #                 numpoints=1,
    #                 bbox_to_anchor=(0.5,0.995),
    #                 loc=9,
    #                 ncol=2,  borderaxespad=0.)

    ### set y limit
    ymax  = np.max(plotIniResY)
    ymax2 = np.max(plotiniResY1)

    ymin  = np.min(plotIniResY)
    ymin2 = np.min(plotiniResY1)

    if (ymax < ymax2):
        ymax = ymax2

    if (ymin > ymin2):
        ymin = ymin2

    axArr[0].set_ylim([ymin*0.1, ymax * 10])

    ### set grid
    axArr[0].grid(b=True, which = 'major' , color = 'k', linestyle='-')
    axArr[0].grid(b=True, which = 'minor' , color = 'k', linestyle='--')

    ### plot final residual for all outer loop and and denotes first and last step of outer loop
    axArr[1].plot(plotX, plotFinResY,'r',linewidth=lnWidth)
    axArr[1].plot(simulTime, plotfinResY1,'ys',markersize=mkSize,linewidth=lnWidth)

    ### set y log scale plot
    axArr[1].set_yscale('log')

    ### set axis label
    axArr[1].set_ylabel('Final Res.',fontsize=fntSize)

    ### set legend
    axArr[1].legend(["Residual","Time  Loop Start Point"],
                    numpoints=1,
                    bbox_to_anchor=(0.5,1.14),
                    loc=9,
                    ncol=2,  borderaxespad=0.)

    ### set y limit
    ymax  = np.max(plotFinResY)
    ymax2 = np.max(plotfinResY1)

    ymin  = np.min(plotFinResY)
    ymin2 = np.min(plotfinResY1)

    if (ymax < ymax2):
        ymax = ymax2

    if (ymin > ymin2):
        ymin = ymin2

    axArr[1].set_ylim([ymin*0.1, ymax * 10])

    ### set grid
    axArr[1].grid(b=True, which = 'major' , color = 'k', linestyle='-')
    axArr[1].grid(b=True, which = 'minor' , color = 'k', linestyle='--')

    ### plot number of outer loop and solver iteration number for first step
    axArr[2].plot(simulTime, nPIMPLE,'yd-',markersize=mkSize,linewidth=lnWidth)
    axArr[2].plot(plotX, plotnSolverY,'k',markersize=mkSize,linewidth=lnWidth)
    axArr[2].plot(simulTime, plotSolY1,'gd',markersize=mkSize,linewidth=lnWidth)

    ### set axis label
    axArr[2].set_xlabel('Time [s]',fontsize=fntSize)
    axArr[2].set_ylabel('n Iter.',fontsize=fntSize)

    ### set legend
    axArr[2].legend(["nPIMPLE", "nSolver", "nSolver : Time Step"],
                    numpoints=1,
                    bbox_to_anchor=(0.5,1.14),
                    loc=9,
                    ncol=3,  borderaxespad=0.)

    ### Set Limitation
    ### set y limit
    ymax  = np.max(nPIMPLE)
    ymax2 = np.max(plotnSolverY)
    ymax3 = np.max(plotSolY1)

    if (ymax < ymax2):
        ymax = ymax2
    if (ymax < ymax3):
        ymax = ymax3

    if (int(ymax * 1.5) <= ymax +2 ): ymax = ymax + 2

    axArr[2].set_ylim([-1, ymax])

    ### set grid
    axArr[2].grid(b=True, which = 'major' , color = 'k', linestyle='-')
    axArr[2].grid(b=True, which = 'minor' , color = 'k', linestyle='--')

    ### figure layout Setting
    fig.tight_layout()

    ### Show the plot
    plt.show()
