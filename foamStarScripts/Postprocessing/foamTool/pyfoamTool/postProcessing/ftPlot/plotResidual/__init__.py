#!/usr/bin/env python
###
###     foamBazar
###
###     Python Package : "plotResidual"
###
###         it includes the modules to plot residual of OpenFOAM log
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 27th Feb. 2017
###

import plotResidualSimple
