#!/usr/bin/env python
###
###     ecfoamTool
###
###     Python Main Program : plotResidual
###
###         It plots residual of OpenFOAM log.
###
###     To run :
###
###         (1)  python plotResidual "variableName" (default : /logResult)
###
###         (2)  python plotResidual "variableName" "LogResultFolderPath"
###
###         Be careful, the input path of this function is directory
###         which includes massError.*
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 27th Feb. 2017
###

### Python Module
import os
import sys

### User Defined Module
import plotResidualSimple
import plotResidualAll

### Main Program
if __name__ == "__main__" :

    casePath = os.getcwd()
    Narg = len(sys.argv)

    isSimplePlot = True
    isAllPlot = False

    logFolderPath = ""
    variableName  = ""

    argActive = [0] * Narg
    argActive[0] = 1

    for ii in range (0, Narg):

        if (sys.argv[ii] == "-a" or sys.argv[ii] == "-A" or
            sys.argv[ii] == "-all" or sys.argv[ii] == "-All"):

            isSimplePlot = False
            isAllPlot = True
            argActive[ii] = 1

        if (sys.argv[ii] == "-d" or sys.argv[ii] == "-D" or
            sys.argv[ii] == "-dir" or sys.argv[ii] == "-Dir" ):

            argActive[ii]   = 1
            argActive[ii+1] = 1

            logFolderPath  = sys.argv[ii+1]

            if (logFolderPath.endswith('/')):
                logFolderPath=logFolderPath[:-1]

            if (not logFolderPath.startswith('/')):
                logFolderPath="/"+logFolderPath

        if (sys.argv[ii] == "-v" or sys.argv[ii] == "-var" or
            sys.argv[ii] == "-Var" or sys.argv[ii] == "-V"):

            argActive[ii]   = 1
            argActive[ii+1] = 1
            variableName = sys.argv[ii+1]

    if (variableName == "") :
        variableName = input("   Please Enter the Variable Name : ")

    if (logFolderPath == "") :
        logFolderPath = "/logResult"

    ### Input file path and variable name
    fileDirPath  = casePath
    fileDirPath += logFolderPath

    ### Call plot function
    if (isSimplePlot):
        plotResidualSimple.fbplotResidualSimple(fileDirPath, variableName)

    if (isAllPlot):
        plotResidualAll.fbplotResidualAll(fileDirPath, variableName)
