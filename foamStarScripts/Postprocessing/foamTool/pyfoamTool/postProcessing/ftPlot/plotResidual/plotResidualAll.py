#!/usr/bin/env python
###
###     foamBazar
###
###     Python Script : plotResidualSimple.py
###
###         Python function to plot Residual for Outer Loop
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 27th Feb. 2017
###

### Python Module
import os
import matplotlib.pyplot as plt
import numpy as np
import glob

### User Defined Module
import ftRead

### function to plot residual for given log directory
def fbplotResidualAll(logFolderPath, variableName) :

    testPath1 =logFolderPath
    testPath1+="/initialResidual.*."
    testPath1+=variableName

    testPath2 =logFolderPath
    testPath2+="/finalResidual.*."
    testPath2+=variableName

    testPath3 =logFolderPath
    testPath3+="/nIteration.*."
    testPath3+=variableName

    ### Compter maximum number of Inner Loop
    initialFiles = glob.glob(testPath1)
    finalFiles = glob.glob(testPath2)
    nLoopFiles = glob.glob(testPath3)

    nInitialFiles = len(initialFiles)
    nFinalFiles = len(finalFiles)
    nNLoopFiles = len(nLoopFiles)

    if (nInitialFiles == nFinalFiles and nFinalFiles == nNLoopFiles and nNLoopFiles == nInitialFiles):

        nLoop = nInitialFiles

        initialResidual=[]
        finalResidual=[]
        nIteration=[]

        maxIter = []
        simulTime=[]
        nPIMPLE = []

        iniResData = []
        finResData = []
        nSolver = []

        for iloop in range(0, nLoop) :
            ### extracted residual file name from foamStar log extractor
            initialResidualPath = logFolderPath
            finalResidualPath   = logFolderPath
            nIterResidualPath   = logFolderPath

            initialResidualPath+= "/initialResidual."
            finalResidualPath  += "/finalResidual."
            nIterResidualPath  += "/nIteration."

            initialResidualPath+= str(iloop)
            finalResidualPath  += str(iloop)
            nIterResidualPath  += str(iloop)

            initialResidualPath+= "."
            finalResidualPath  += "."
            nIterResidualPath  += "."

            initialResidualPath+= variableName
            finalResidualPath  += variableName
            nIterResidualPath  += variableName

            ### load file
            initialResidual.append(ftRead.varData(initialResidualPath))
            finalResidual.append(ftRead.varData(finalResidualPath))
            nIteration.append(ftRead.varData(nIterResidualPath))

            ### set variables to plot
            maxIter.append(initialResidual[iloop].nVariables() - 2)
            simulTime.append(initialResidual[iloop].colData(0))
            nPIMPLE.append(initialResidual[iloop].colData(1))

            iniResData.append(initialResidual[iloop].rowData())
            finResData.append(finalResidual[iloop].rowData())
            nSolver.append(nIteration[iloop].rowData())

        ###


        ### set temporal variables to plot (OuterLoop)
        plotX        = []
        plotIniResY  = []
        plotFinResY  = []

        plotnSolverY  = []

        plotiniResY1 = initialResidual[0].colData(2)
        plotfinResY1 = finalResidual[0].colData(2)
        plotSolY1 = nIteration[0].colData(2)

        plotX2    = []
        plotSolY2 = []
        plotiniResY2 = []
        plotfinResY2 = []

        nTimeStep = len(simulTime[0])

        for it in range(0, nTimeStep-1):
            nIter  = int(nPIMPLE[0][it])

            deltaT = (simulTime[0][it + 1] - simulTime[0][it]) / (nIter) / (nLoop)

            curTime=simulTime[0][it]

            for ii in range(0, nIter):

                plotX2.append(curTime)

                plotiniResY2.append(iniResData[0][it][2+ii])
                plotfinResY2.append(finResData[0][it][2+ii])
                plotSolY2.append(nSolver[0][it][2+ii])

                for iloop in range (0, nLoop):

                    plotX.append(curTime)

                    plotIniResY.append(iniResData[iloop][it][2+ii])
                    plotFinResY.append(finResData[iloop][it][2+ii])
                    plotnSolverY.append(nSolver[iloop][it][2+ii])

                    curTime = curTime + deltaT

        ### Plot Setting
        mkSize  = 8
        fntSize = 18
        lnWidth = 2

        ### set figure default setting
        fig, axArr = plt.subplots(3, sharex = True , figsize = (12,10), facecolor ='w', edgecolor='k')

        ### set title
        titleName = "Residual :"
        titleName +=variableName
        axArr[0].set_title(titleName)

        ### plot initial residual for all outer loop and and denotes first and last step of outer loop
        axArr[0].plot(plotX, plotIniResY,'b',linewidth=lnWidth)
        axArr[0].plot(plotX2, plotiniResY2,'ro',markersize=mkSize,linewidth=lnWidth)
        axArr[0].plot(simulTime[0][:], plotiniResY1,'go',markersize=mkSize,linewidth=lnWidth)

        ### set y log scale plot
        axArr[0].set_yscale('log')

        ### set axis label
        axArr[0].set_ylabel('Initial Res.',fontsize=fntSize)

        ### set y limit
        ymax  = np.max(plotIniResY)
        ymax2 = np.max(plotiniResY2)
        ymax3 = np.max(plotiniResY1)

        ymin  = np.min(plotIniResY)
        ymin1 = np.min(plotiniResY2)
        ymin2 = np.min(plotiniResY1)

        if (ymax < ymax2):
            ymax = ymax2
        if (ymax < ymax3):
            ymax = ymax3

        if (ymin > ymin1):
            ymin = ymin1
        if (ymin > ymin2):
            ymin = ymin2

        axArr[0].set_ylim([ymin*0.1, ymax * 10])

        ### set legend
        # axArr[0].legend(["Initial Residual","Outer Loop Start Point","Time  Loop Start Point"],
        #                 numpoints=1,
        #                 bbox_to_anchor=(0.5,0.995),
        #                 loc=9,
        #                 ncol=3,  borderaxespad=0.)

        ### set grid
        axArr[0].grid(b=True, which = 'major' , color = 'k', linestyle='-')
        # axArr[0].grid(b=True, which = 'minor' , color = 'k', linestyle='--')

        ### plot final residual for all outer loop and and denotes first and last step of outer loop
        axArr[1].plot(plotX, plotFinResY,'b')
        axArr[1].plot(plotX2, plotfinResY2,'rs',markersize=mkSize,linewidth=lnWidth)
        axArr[1].plot(simulTime[0][:], plotfinResY1,'gs',markersize=mkSize,linewidth=lnWidth)

        ### set y log scale plot
        axArr[1].set_yscale('log')

        ### set axis label
        axArr[1].set_ylabel('Final Res.',fontsize=fntSize)

        ### set y limit
        ymax  = np.max(plotFinResY)
        ymax2 = np.max(plotfinResY2)
        ymax3 = np.max(plotfinResY1)

        ymin  = np.min(plotFinResY)
        ymin1 = np.min(plotfinResY2)
        ymin2 = np.min(plotfinResY1)

        if (ymax < ymax2):
            ymax = ymax2
        if (ymax < ymax3):
            ymax = ymax3

        if (ymin > ymin1):
            ymin = ymin1
        if (ymin > ymin2):
            ymin = ymin2

        axArr[1].set_ylim([ymin*0.1, ymax * 10])

        ### set legend
        axArr[1].legend(["Residual","Outer Loop Start Point","Time  Loop Start Point"],
                        numpoints=1,
                        bbox_to_anchor=(0.5,1.14),
                        loc=9,
                        ncol=3,  borderaxespad=0.)

        ### set grid
        axArr[1].grid(b=True, which = 'major' , color = 'k', linestyle='-')
        # axArr[1].grid(b=True, which = 'minor' , color = 'k', linestyle='--')

        ### plot number of outer loop and solver iteration number for first step
        axArr[2].plot(simulTime[0], nPIMPLE[0],'yd-',markersize=mkSize,linewidth=lnWidth)
        axArr[2].plot(plotX, plotnSolverY,'k',markersize=mkSize,linewidth=lnWidth)
        axArr[2].plot(plotX2, plotSolY2,'rd',markersize=mkSize,linewidth=lnWidth)
        axArr[2].plot(simulTime[0][:], plotSolY1,'gd',markersize=mkSize,linewidth=lnWidth)

        ### set axis label
        axArr[2].set_xlabel('Time [s]',fontsize=fntSize)
        axArr[2].set_ylabel('n Iter.',fontsize=fntSize)

        ### set legend
        axArr[2].legend(["nPIMPLE","nSolver","nSolver : Outer Loop ","nSolver : Time Loop"],
                        numpoints=1,
                        bbox_to_anchor=(0.5,1.14),
                        loc=9,
                        ncol=4,
                        borderaxespad=0.)

        ### set y limit
        ymax  = np.max(nPIMPLE[0])
        ymax2 = np.max(plotnSolverY)
        ymax3 = np.max(plotSolY2)
        ymax4 = np.max(plotSolY1)

        if (ymax < ymax2):
            ymax = ymax2
        if (ymax < ymax3):
            ymax = ymax3
        if (ymax < ymax4):
            ymax = ymax4

        if (int(ymax * 1.5) <= ymax + 2 ):
            ymax = ymax + 2
        else:
            ymax = int(ymax * 1.5)

        axArr[2].set_ylim([-1, ymax])

        ### set grid
        axArr[2].grid(b=True, which = 'major' , color = 'k', linestyle='-')
        # axArr[2].grid(b=True, which = 'minor' , color = 'k', linestyle='--')

        ### figure layout Setting
        fig.tight_layout()

        ### Show the plot
        plt.show()
