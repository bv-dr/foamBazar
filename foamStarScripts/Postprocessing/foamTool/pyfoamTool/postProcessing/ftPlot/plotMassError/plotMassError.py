#!/usr/bin/env python
###
###     foamBazar
###
###     Python Script : plotMassError.py
###
###         Python function to plot Continuity Error
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 27th Feb. 2017
###

### Python Module
import matplotlib.pyplot as plt
import numpy as np

### User Defined Module
import ftRead

### function to plot Mass Error for given continuity error directory
def fbplotMassError(logDirPath) :

    ### extracted mass error file name from foamStar log extractor
    massErrorLocalFileName      = "/massError.localSum"
    massErrorGlobalFileName     = "/massError.globalSum"
    massErrorCumulativeFileName = "/massError.cumulative"

    ### set file path
    massErrorLocalFilePath = logDirPath
    massErrorLocalFilePath+= massErrorLocalFileName

    massErrorGlobalFilePath = logDirPath
    massErrorGlobalFilePath+= massErrorGlobalFileName

    massErrorCumulativeFilePath    = logDirPath
    massErrorCumulativeFilePath   += massErrorCumulativeFileName

    ### load file
    massErrorLocalFile      = ftRead.varData(massErrorLocalFilePath)
    massErrorGlobalFile     = ftRead.varData(massErrorGlobalFilePath)
    massErrorCumulativeFile = ftRead.varData(massErrorCumulativeFilePath)

    ### set variables to plot
    simulTime = massErrorLocalFile.colData(0)
    nPIMPLE   = massErrorLocalFile.colData(1)

    massErrorLocalData = massErrorLocalFile.rowData()
    massErrorGlobalData = massErrorGlobalFile.rowData()

    massErrorCumulative = massErrorCumulativeFile.colData(1)

    ### set temporal variables to plot (OuterLoop)
    plotX                = []
    plotmassErrorLocalY  = []
    plotmassErrorGlobalY = []

    plotmassErrorLocalY1  = massErrorLocalFile.colData(2)
    plotmassErrorGlobalY1 = massErrorGlobalFile.colData(2)

    nTimeStep = len(simulTime)
    for it in range(0, nTimeStep-1):
        nIter  = int(nPIMPLE[it])

        deltaT = (simulTime[it + 1] - simulTime[it]) / nIter
        curTime=simulTime[it]

        for ii in range(0, nIter):
            plotX.append(curTime)

            plotmassErrorLocalY.append(massErrorLocalData[it][2+ii])
            plotmassErrorGlobalY.append(massErrorGlobalData[it][2+ii])

            curTime = curTime + deltaT

    ### Plot Setting
    mkSize  = 8
    fntSize = 18
    lnWidth = 2

    ### set figure default setting
    fig, axArr = plt.subplots(2, sharex = True , figsize = (12,9), facecolor ='w', edgecolor='k')

    ### set title
    titleName ="Continuity Error Result Plot"
    axArr[0].set_title(titleName)

    ### plot the graph - sub plot 211
    axArr[0].plot(plotX, plotmassErrorLocalY,'g-',markersize=mkSize,linewidth=lnWidth)
    axArr[0].plot(plotX, plotmassErrorGlobalY,'b-',markersize=mkSize,linewidth=lnWidth)

    axArr[0].plot(simulTime, plotmassErrorLocalY1,'ro',markersize=mkSize,linewidth=lnWidth)
    axArr[0].plot(simulTime, plotmassErrorGlobalY1,'yd',markersize=mkSize,linewidth=lnWidth)

    ### set ylabel for subplot 211
    axArr[0].set_ylabel('Local, Global Mass Error',fontsize=fntSize)

    ### set legend for sub plot 211
    axArr[0].legend(["Local Error","Global Error","Initial Error : Time Start", "Final Error : Time Start"],
                numpoints=1,
                bbox_to_anchor=(0.5,0.995),
                loc=9,
                ncol=4,  borderaxespad=0.)

    ### set grid for sub plot 211
    axArr[0].grid(b=True, which = 'major' , color = 'k', linestyle='-')
    axArr[0].grid(b=True, which = 'minor' , color = 'k', linestyle='--')

    ### plot the graph - sub plot 211
    axArr[1].plot(simulTime, massErrorCumulative,'k-',markersize=mkSize,linewidth=lnWidth)

    ### set axis label
    axArr[1].set_xlabel('Time [s]',fontsize=fntSize)
    axArr[1].set_ylabel('Cumulative Mass Error',fontsize=fntSize)

    ### set legend for sub plot 212
    axArr[1].legend(["Cumulative"],
                    bbox_to_anchor=(0.005,0.005),
                    loc=3,
                    ncol=1,  borderaxespad=0.)

    ### set grid for sub plot 212
    axArr[1].grid(b=True, which = 'major' , color = 'k', linestyle='-')
    axArr[1].grid(b=True, which = 'minor' , color = 'k', linestyle='--')

    ### figure layout Setting
    fig.tight_layout()

    ### Show the plot
    plt.show()
