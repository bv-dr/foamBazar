#!/usr/bin/env python
###
###     foamBazar
###
###     Python Package : "plotMassError"
###
###         it includes the modules to plot continuity Error of foamStar log
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 27th Feb. 2017
###

import plotMassError
