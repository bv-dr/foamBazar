#!/usr/bin/env python
###
###     foamBazar
###
###     Python Main Program : plotCourant
###
###         It plots Courant number of foamStar log.
###
###     To run :
###
###         (1)  python plotCourant (default : /logResult)
###
###         (2)  python plotCourant "LogDirectoryPath"
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 27th Feb. 2017
###

### Python Module
import os
import sys

### User Defined Module
import plotCourant

### Main Program
if __name__ == "__main__" :

    ### Test input argument and set file path from argument and option to plot
    casePath = os.getcwd()
    Narg = len(sys.argv)

    logFolderPath = ""
    variableName  = ""

    argActive = [0] * Narg
    argActive[0] = 1

    for ii in range (0, Narg):

        if (sys.argv[ii] == "-d" or sys.argv[ii] == "-D" or
            sys.argv[ii] == "-dir" or sys.argv[ii] == "-Dir" ):

            argActive[ii]   = 1
            argActive[ii+1] = 1

            logFolderPath  = sys.argv[ii+1]

            if (logFolderPath.endswith('/')):
                logFolderPath=logFolderPath[:-1]

            if (not logFolderPath.startswith('/')):
                logFolderPath="/"+logFolderPath

    if (logFolderPath == "") :
        logFolderPath = "/logResult"

    ### Input file path and variable name
    logDirPath  = casePath
    logDirPath += logFolderPath

    ### Call plot function
    plotCourant.fbplotCourantNumber(logDirPath)
