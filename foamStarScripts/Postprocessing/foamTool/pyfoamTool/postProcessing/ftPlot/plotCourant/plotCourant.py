#!/usr/bin/env python
###
###     foamBazar
###
###     Python Script : plotCourant.py
###
###         Python function to plot Courant Number
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 27th Feb. 2017
###

### Python Module
import matplotlib.pyplot as plt
import numpy as np

### User Defined Module
import ftRead

### function to plot Courant Number for given courant number directory path
def fbplotCourantNumber(logFolderDirPath) :

    CourantFileName = "/CourantNumber"

    CourantFilePath = logFolderDirPath
    CourantFilePath+= CourantFileName

    ### read file
    CourantNumberFile = ftRead.varData(CourantFilePath)

    ### set variables to plot
    simulTime    = CourantNumberFile.colData(0)
    CoMean       = CourantNumberFile.colData(1)
    CoMax        = CourantNumberFile.colData(2)
    InterCoMean  = CourantNumberFile.colData(3)
    InterCoMax   = CourantNumberFile.colData(4)

    ### Plot Setting
    mkSize  = 8
    fntSize = 18
    lnWidth = 2

    ### set figure default setting
    fig = plt.figure(num = None, figsize=(12, 6), facecolor='w', edgecolor='k')

    ### set title
    titleName ="Courant Number Plot"
    plt.title(titleName)

    ### plot the graph
    plt.plot(simulTime, CoMean,'bo-',markersize=mkSize,linewidth=lnWidth)
    plt.plot(simulTime, CoMax,'ro-',markersize=mkSize,linewidth=lnWidth)
    plt.plot(simulTime, InterCoMean,'md-',markersize=mkSize,linewidth=lnWidth)
    plt.plot(simulTime, InterCoMax,'gd-',markersize=mkSize,linewidth=lnWidth)

    ### set axis label
    plt.xlabel('Time [s]',fontsize=fntSize)
    plt.ylabel('Courant Number',fontsize=fntSize)

    ### set legend of data
    plt.legend(["Mean Co","Max Co","inter Co Mean","inter Co Max"],
            numpoints=1,
            bbox_to_anchor=(0.5,0.995),
            loc=9,
            ncol=4,  borderaxespad=0.)

    ### set y limit
    ymax  = np.max(CoMean)
    ymax2 = np.max(CoMax)
    ymax3 = np.max(InterCoMean)
    ymax4 = np.max(InterCoMax)

    if (ymax < ymax2):
        ymax = ymax2
    if (ymax < ymax3):
        ymax = ymax3
    if (ymax < ymax4):
        ymax = ymax4


    plt.ylim([-0.05, ymax*1.5])

    ### set grid
    plt.grid(b=True, which = 'major' , color = 'k', linestyle='-')
    plt.grid(b=True, which = 'minor' , color = 'k', linestyle='--')

    ### figure layout Setting
    plt.tight_layout()

    ### Show the plot
    plt.show()
