#!/usr/bin/env python
###
###     ecfoamTool
###
###     Python Package : "plotCourant"
###
###         it includes the modules to plot Courant Number of OpenFOAM log
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 27th Feb. 2017
###

import plotCourant
