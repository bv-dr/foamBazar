#!/usr/bin/env python
###
###     ecfoamTool
###
###     : ecfoamTool shell Script to plot the result of openFOAM
###
###     It calls python package which is named as ftPlot
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 27th Feb. 2017
###

### foam Tool Plot Directory
export ftPlotDir=$ftPostProCessing/"ftPlot"

### plot residual
function ftplotResidual()
{
    python $ftPlotDir/"plotResidual" $@
}

### plot Courant Number
function ftplotCourant()
{
    python $ftPlotDir/"plotCourant" $@
}

### plot Volume of Fraction
function ftplotVOF()
{
    python $ftPlotDir/"plotVOF" $@
}

### plot Continuity Error
function ftplotMassError()
{
    python $ftPlotDir/"plotMassError" $@
}
