#!/bin/bash
###
###  ecfoamTool :
###
###  This script includes those functions
###
###     gater [option same as find]
###          : gather directory or file from given find option
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 24th Feb. 2017
###

promptyn()
{
    while true; do
        read -p "$1 " yn
        case $yn in
            [Yy]* ) return 0;;
            [Nn]* ) return 1;;
            * )
            echo " "
            echo "          Please answer yes or no. "
            echo " ";;
        esac
    done
}

function gather()
{
    local activeIndex="0"
    local isDir="0"
    local isFile="1"
    local searchingName=""

    for arg in $@
    do
        if [ $activeIndex == "1" ]; then
            local searchingName=$(echo "$arg")
            break
        fi

        if [ $arg == "-name" ]; then
            local activeIndex="1"
        fi

        if [ $arg == "d" ]; then
            local isDir="1"
            local isFile="0"
        fi

        if [ $arg == "f" ]; then
            local isDir="0"
            local isFile="1"
        fi

    done

    if [ $searchingName == "" ]; then
        echo " "
        echo "[ERROR] No file or directory Name is given !!! "
        echo " "
    else

        if [ $isDir == "1" ]; then
            echo " "
            echo " Directory : "$searchingName " will be Gathered."
            echo " "
        fi

        if [ $isFile == "1" ]; then
            echo " "
            echo " File      : " $searchingName " will be Gathered."
            echo " "
        fi

        local gatheredFolder="data-"$searchingName

        if [ ! -d "$gatheredFolder" ]; then
            mkdir "$gatheredFolder"
        else

            echo " [WARNING] Directory for Gathering Exists !! : " $searchingName
            echo " "

            if promptyn "    Do you want to delete directory ? " ; then
                rm -rf $gatheredFolder
                echo " "
                echo "     Directory is deleted : " $searchingName
                echo " "
                mkdir "$gatheredFolder"
            else
                echo " "
                echo "     gather is paused !!! : "
                echo " "
                return
            fi
        fi

        for file in $(find $@);
        do
            local caseName=$(echo $file | cut -d"/" -f2)
            local searchedFileName=$(basename $file)
            local caseOutputName=$caseName"_"$searchedFileName
            local caseOutputPath=$gatheredFolder/$caseOutputName

            echo "       $file is Now copying ..."

            cp -r $file $caseOutputPath

        done

        echo " "
        echo "     gather is done !!! "
        echo " "

    fi


}
