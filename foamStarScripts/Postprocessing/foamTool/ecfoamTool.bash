#!/bin/bash
###
###  ecfoamTool Script for holding functions
###
###     This script is for the general desscription and relation of ecfoamTool
###     To use ecfoamTool, please add this script on your .bashrc and also set
###     the environmental variable $ECFOAM_TOOL (path of ecfoamTool)
###
###     example) .bashrc
###
###         export ECFOAMTOOL=$HOME/Utilities/ecfoamTool.bash
###         source $ECFOAMTOOL/"ecfoamTool.bash"
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 24th Feb. 2017
###

### Log Extract Source
export ftLogExtractDir=$ECFOAMTOOL/"logExtract"

source $ftLogExtractDir/"foamStarLogExtract.bash"
source $ftLogExtractDir/"navalFoamLogExtract.bash"

### Gather Data source
export ftGatherDataDir=$ECFOAMTOOL/"gatherData"
source $ftGatherDataDir/"ftGatherData.bash"

### pyFoamBazar
export pyFOAMTOOL=$ECFOAMTOOL/"pyfoamTool"
source $pyFOAMTOOL/"pyfoamTool.bash"
