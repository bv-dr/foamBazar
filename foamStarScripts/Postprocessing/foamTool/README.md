# foamTool

OpenFOAM ToolKit for simulation results analysis developed by YoungMyung Choi


Example 
    export ECFOAMTOOL=$PATH_TO_FOAMTOOL/foamTool
    source $ECFOAMTOOL/ecfoamTool.bash
    alias ft="cd $ECFOAMTOOL"

Example of gathering
    # ft commend recommended to use
    alias ge="gather -name surfaceElevation.dat"
    alias gf="gather -name forces.dat"
    alias gs="gather -name ship.dat"
