BEGIN{
      iter = -1;
      iTime = -1;
      maxIter = 0;
}
{
    if ($1=="Time" && $2=="=")
    {
        iTime++;
        Time[iTime] = $3;
    }

    if (iTime > -1)
    {
        if ($1=="PIMPLE:" && $2=="iteration")
        {
            iterPIMPLE = $3
            if (iterPIMPLE=="1")
            {
                if (maxIter < iter)
                {
                    maxIter=iter
                }
                iter = 0
            }
            else
            {
                iter++
                Niter[iTime] = iter;
            }
        }

        if ($1=="Phase-1" && $2=="volume" && $3=="fraction")
        {
            VOF[iTime,iter]=$5
            VOFMin[iTime,iter]=$8
            VOFMax[iTime,iter]=$11
        }
    }
}
END{
    VOFSimpleOutPath = outFolder"/VOF.simple"

    VOFOutPath       = outFolder"/VOF.vof"
    VOFMinOutPath    = outFolder"/VOF.min"
    VOFMaxOutPath    = outFolder"/VOF.max"

    minVOF =  1e6;
    maxVOF = -1e6;

    for (it = 0; it<iTime+1; it++)
    {
        for (iter =0; iter < Niter[it]+1; iter++)
        {
            if (minVOF < VOFMin[it,iter])
            {
                minVOF = VOFMin[it,iter];
            }

            if (maxVOF > VOFMax[it,iter])
            {
                maxVOF = VOFMax[it,iter];
            }
        }
    }

    printf "variables= time nIter" > VOFSimpleOutPath
    printf "variables= time nIter" > VOFOutPath
    printf "variables= time nIter" > VOFMinOutPath
    printf "variables= time nIter" > VOFMaxOutPath


    printf "       VOFMin        VOFMax           VOF" > VOFSimpleOutPath

    for (iter=0; iter<maxIter+1;iter++)
    {
        printf "      VOFMin%1d ",iter+1 > VOFMinOutPath
        printf "      VOFMax%1d ",iter+1 > VOFMaxOutPath
        printf "         VOF%1d ",iter+1 > VOFOutPath
    }

    printf "\n" > VOFSimpleOutPath
    printf "\n" > VOFOutPath
    printf "\n" > VOFMinOutPath
    printf "\n" > VOFMaxOutPath

    printf "#           [-]  [-] " > VOFSimpleOutPath
    printf "#           [-]  [-] " > VOFOutPath
    printf "#           [-]  [-] " > VOFMinOutPath
    printf "#           [-]  [-] " > VOFMaxOutPath

    printf "          [-]           [-]           [-]     " > VOFSimpleOutPath

    for (iter=0; iter<maxIter+1;iter++)
    {
        printf "          [-] " > VOFOutPath
        printf "          [-] " > VOFMinOutPath
        printf "          [-] " > VOFMaxOutPath
    }

    printf "\n" > VOFSimpleOutPath
    printf "\n" > VOFOutPath
    printf "\n" > VOFMinOutPath
    printf "\n" > VOFMaxOutPath

    for (it = 0; it<iTime; it++)
    {
        printf "%15.7f %4d",Time[it],Niter[it]+1 > VOFSimpleOutPath
        printf "%15.7f %4d",Time[it],Niter[it]+1 > VOFOutPath
        printf "%15.7f %4d",Time[it],Niter[it]+1 > VOFMinOutPath
        printf "%15.7f %4d",Time[it],Niter[it]+1 > VOFMaxOutPath

        for (iter =0; iter < Niter[it]+1; iter++)
        {
            printf " %13.6e",VOF[it,iter] > VOFOutPath
            printf " %13.6e",VOFMin[it,iter] > VOFMinOutPath
            printf " %13.6e",VOFMax[it,iter] > VOFMaxOutPath
        }

        for (iter = Niter[it]+1; iter<maxIter+1; iter++)
        {
            printf " %13.6e", 0.0 > VOFOutPath
            printf " %13.6e", 0.0 > VOFMinOutPath
            printf " %13.6e", 0.0 > VOFMinOutPath
        }

        printf " %13.6e %13.6e %13.6e",VOFMin[it,Niter[it]],VOFMax[it,Niter[it]],VOF[it,Niter[it]] > VOFSimpleOutPath

        printf "\n" > VOFSimpleOutPath
        printf "\n" > VOFOutPath
        printf "\n" > VOFMinOutPath
        printf "\n" > VOFMaxOutPath

    }
}
