BEGIN{
      iter = -1;
      iTime = -1;
      maxIter = 0;
      residualVariable[0] = "U";
      residualVariable[1] = "p_rgh";
      residualVariable[1] = "pc";
}
{
    if ($1=="Time" && $2=="=")
    {
        iTime++;
        Time[iTime] = $3;
    }

    if (iTime > -1)
    {
        if ($1=="PIMPLE:" && $2=="iteration")
        {
            iterPIMPLE = $3
            if (iterPIMPLE=="1")
            {
                if (maxIter < iter)
                {
                    maxIter=iter
                }
                iter = 0
            }
            else
            {
                iter++
                Niter[iTime] = iter;
            }
        }

        if ($1=="fsPIMPLE" && $2=="Field")
        {
            logVariable = $4;
            for (ivar in residualVariable)
            {
                if (logVariable == residualVariable[ivar])
                {
                    initialResidual[ivar,iTime,iter] = $8;
                    residual[ivar,iTime,iter] = $11;
                    residualDiff[ivar,iTime,iter] = $15;
                    relativeResDiff[ivar,iTime,iter] = $20;
                }
            }

        }
    }
}
END{
    for (ivar in residualVariable)
    {
        for (ii = 0; ii < 4; ii++)
        {
            if (ii == 0) { OutPath = outFolder"/resInit."residualVariable[ivar]}
            else if (ii == 1) { OutPath = outFolder"/resAbs."residualVariable[ivar]}
            else if (ii == 2) { OutPath = outFolder"/resPrev."residualVariable[ivar]}
            else if (ii == 3) { OutPath = outFolder"/resRelPrev."residualVariable[ivar]}

            printf "# foamStar log extracted data \n#\n" > OutPath
            printf "#   Data Type  : Residual \n" > OutPath
            printf "#   Variable   : %s \n",residualVariable[ivar] > OutPath
            printf "# \n" > OutPath

            printf "variables= time nIter" > OutPath
            for (iter=0; iter<maxIter+1;iter++)
            {
                if (ii == 0) {printf "     resInit%1d",iter+1 > OutPath}
                if (ii == 1) {printf "    residual%1d",iter+1 > OutPath}
                if (ii == 2) {printf "     resPrev%1d",iter+1 > OutPath}
                if (ii == 3) {printf "  resRelPrev%1d",iter+1 > OutPath}
            }
            printf "\n" > OutPath

            printf "#           [-]   [-]" > OutPath
            for (iter=0; iter<maxIter+1;iter++)
            {
                printf "          [-]" > OutPath
            }
            printf "\n" > OutPath

            for (it = 0; it<iTime; it++)
            {
                printf "%15.7f %4d",Time[it],Niter[it]+1 > OutPath

                for (iter =0; iter < Niter[it]+1; iter++)
                {
                    if (ii == 0) {printf " %13.6e",initialResidual[ivar,it,iter] > OutPath}
                    if (ii == 1) {printf " %13.6e",residual[ivar,it,iter] > OutPath}
                    if (ii == 2) {printf " %13.6e",residualDiff[ivar,it,iter] > OutPath}
                    if (ii == 3) {printf " %13.6e",relativeResDiff[ivar,it,iter] > OutPath}
                }

                for (iter = Niter[it]+1; iter<maxIter+1; iter++)
                {
                    printf " %13.6e", -1.0 > OutPath
                }
                printf "\n" > OutPath
            }
        }
    }

}
