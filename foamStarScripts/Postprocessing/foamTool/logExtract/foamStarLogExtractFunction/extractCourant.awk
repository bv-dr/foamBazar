BEGIN{
        iTime = -1;
     }
{
    if ($1=="Create" && $2=="mesh" && $3=="for" && $4=="time")
    {
        iTime++;
        Time[iTime] = $6;
    }

    if ($1=="Time" && $2=="=")
    {
        iTime++;
        Time[iTime] = $3;
    }

    if ($1=="Interface" && $2=="Courant" && $3=="Number" && $4=="mean:")
    {
        InterCourantNoMean[iTime] = $5
        InterCourantNoMax[iTime]  = $7
    }

    if ($1=="Courant" && $2=="Number" && $3=="mean:")
    {
        CourantNoMean[iTime] = $4
        CourantNoMax[iTime]  = $6
    }

}
END{
        maxCourant = -1e6;
        maxIntefaceCourant = -1e6;

        for (it = 0; it<iTime; it++)
        {
            if (maxCourant < CourantNoMax[it])
            {
                maxCourant = CourantNoMax[it];
            }

            if (maxIntefaceCourant < InterCourantNoMax[it])
            {
                maxIntefaceCourant = InterCourantNoMax[it];
            }
        }

        printf "# foamStar log extracted data \n#\n"
        printf "#   Data Type : Courant Number \n"
        printf "#   Variable  : Courant Number \n#\n"
        printf "#   Max. Courant Number          : %13.6e \n", maxCourant
        printf "#   Max. Inteface Courant Number : %13.6e \n", maxIntefaceCourant

        printf "# \n"

        print "variables= time        CoMean         CoMax   InterCoMean    InterCoMax"
	    print "#           [s]           [-]           [-]           [-]           [-]"

        for (it = 0; it<iTime; it++)
        {
            printf "%15.7f %13.6e %13.6e %13.6e %13.6e \n",
                    Time[it], CourantNoMean[it], CourantNoMax[it],
                    InterCourantNoMean[it], InterCourantNoMax[it]
        }

    }
