BEGIN{
        iTime = -1;
     }
{
    if ($1=="Create" && $2=="mesh" && $3=="for" && $4=="time")
    {
        iTime++;
        Time[iTime] = $6;
    }

    if ($1=="Time" && $2=="=")
    {
        iTime++;
        Time[iTime] = $3;
    }


    if ($1=="YM" && $2=="Probe" )
    {
        value[iTime] = $4
    }
}
END{
        maxValue = -1e6;
        minValue =  1e6;

        for (it = 0; it<iTime; it++)
        {
            if (maxValue < value[it])
            {
                maxValue = value[it];
            }

            if (minValue > value[it])
            {
                minValue = value[it];
            }
        }

        printf "# foamStar log extracted data \n#\n"
        printf "#   Data Type : User Defined Variable \n"
        printf "#   Max. Value : %13.6e \n", maxValue
        printf "#   Max. Value : %13.6e \n", minValue

        printf "# \n"

        print "variables= time          var1"
	    print "#           [s]           [-]"

        for (it = 0; it<iTime; it++)
        {
            printf "%15.7f %13.6e\n",
                    Time[it], value[it]
        }

    }
