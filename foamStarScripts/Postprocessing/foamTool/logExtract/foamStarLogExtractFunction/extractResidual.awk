BEGIN{
      iter = -1;
      iTime = -1;
      maxIter = 0;
      residualVariable[0] = "alpha.water";
      residualVariable[1] = "p_rgh";
      residualVariable[2] = "Ux";
      residualVariable[3] = "Uz";
      residualVariable[4] = "pc";

      for (ivar in residualVariable)
      {
          variableString[ivar]=residualVariable[ivar]",";
          isInitialPISOLoop[ivar]=1;
          isSolverSaved[ivar] = 0;
      }

     }
{
    if ($1=="Time" && $2=="=")
    {
        iTime++;
        Time[iTime] = $3;
    }
    if (iTime > -1)
    {
        if ($1=="PIMPLE:" && $2=="iteration")
        {
            iterPIMPLE = $3

            for (ivar in residualVariable)
            {
                isInitialPISOLoop[ivar] = 1;
            }

            if (iterPIMPLE=="1")
            {
                if (maxIter < iter)
                {
                    maxIter=iter
                }
                iter = 0;
            }
            else
            {
                iter++;
                Niter[iTime] = iter;
            }
        }

        if ($2=="Solving" && $3=="for")
        {
            logVariable = $4;
            for (ivar in residualVariable)
            {
                if (logVariable == variableString[ivar])
                {
                    if (isSolverSaved[ivar] == 0)
                    {
                        solverName[ivar] = substr($1,1,length($1)-1)
                        isSolverSaved[ivar] = 1;
                    }
                    if (isInitialPISOLoop[ivar]==1)
                    {
                        iloop = 0;
                        isInitialPISOLoop[ivar] = 0
                    }
                    else
                    {
                        iloop++
                    }
                    innerLoopNumber[ivar,iTime,iter] = iloop;
                    initialResidual[ivar,iTime,iter,iloop]=$8
                    finalResidual[ivar,iTime,iter,iloop]=$12
                    solverIteration[ivar,iTime,iter,iloop]=$15
                }
            }
        }
    }
}
END{
{
    for (ivar in residualVariable)
    {
        maxInitialRes = -1e6;
        maxFinalRes   = -1e6;

        maxInnerLoop  = 0;
        maxSolverLoop = 0;

        for (it = 0; it<iTime+1; it++)
        {

            if (maxInitialRes < initialResidual[ivar,it,Niter[it],0])
            {
                maxInitialRes = initialResidual[ivar,it,Niter[it],0];
            }

            for (iOuterLoop = 0; iOuterLoop < Niter[it] +1 ; iOuterLoop++)
            {
                if (maxInnerLoop < innerLoopNumber[ivar,it,iOuterLoop])
                {
                    maxInnerLoop = innerLoopNumber[ivar,it,iOuterLoop]
                }

                for (iloop = 0; iloop < innerLoopNumber[ivar,it,iOuterLoop] +1; iloop++)
                {
                    if ( maxSolverLoop < solverIteration[ivar,it,iOuterLoop,iloop])
                    {
                        maxSolverLoop = solverIteration[ivar,it,iOuterLoop,iloop]
                    }

                    if (maxFinalRes < finalResidual[ivar,it,iOuterLoop,iloop])
                    {
                        maxFinalRes = finalResidual[ivar,it,iOuterLoop,iloop];
                    }
                }
            }
        }

        for (iloop=0; iloop < maxInnerLoop + 1; iloop++)
        {

            iniResOutPath = outFolder"/initialResidual."iloop"."residualVariable[ivar];
            finResOutPath = outFolder"/finalResidual."iloop"."residualVariable[ivar];
            nIterOutPath = outFolder"/nIteration."iloop"."residualVariable[ivar];

            printf "# foamStar log extracted data \n#\n" > iniResOutPath
            printf "#   Data Type  : initial Residual \n" > iniResOutPath
            printf "#   Variable   : %s \n",residualVariable[ivar] > iniResOutPath
            printf "#   Inner Loop : %d \n#\n",iloop+1 > iniResOutPath
            printf "#   Solvers    : %s \n#\n",solverName[ivar] > iniResOutPath

            printf "#   Max. Initial Residual at Last Loop : %13.6e \n", maxInitialRes > iniResOutPath
            printf "#   Max. Number of Outer Iteration     : %3d \n",maxIter +1 > iniResOutPath
            printf "#   Max. Number of Inner Iteration     : %3d \n",maxInnerLoop +1 > iniResOutPath
            printf "#   Max. Number of Solver Iteration    : %3d \n",maxSolverLoop > iniResOutPath
            printf "# \n" > iniResOutPath

            printf "# foamStar log extracted data \n#\n" > finResOutPath
            printf "#   Data Type  : final Residual \n" > finResOutPath
            printf "#   Variable   : %s \n",residualVariable[ivar] > finResOutPath
            printf "#   Inner Loop : %d \n#\n",iloop+1 > iniResOutPath
            printf "#   Solvers    : %s \n#\n",solverName[ivar] > finResOutPath

            printf "#   Max. Final Residual                : %13.6e \n", maxFinalRes > finResOutPath
            printf "#   Max. Number of Outer Iteration     : %3d \n",maxIter + 1 > finResOutPath
            printf "#   Max. Number of Inner Iteration     : %3d \n",maxInnerLoop + 1 > finResOutPath
            printf "#   Max. Number of Solver Iteration    : %3d \n",maxSolverLoop > finResOutPath
            printf "# \n" > finResOutPath

            printf "# foamStar log extracted data \n#\n" > nIterOutPath
            printf "#   Data Type : Residual Loop Number \n" > nIterOutPath
            printf "#   Variable   : %s \n",residualVariable[ivar] > finResOutPath
            printf "#   Inner Loop : %d \n#\n",iloop+1 > iniResOutPath
            printf "#   Solvers   : %s \n#\n",solverName[ivar] > nIterOutPath

            printf "#   Max. Number of Outer Iteration     : %3d \n",maxIter + 1 > nIterOutPath
            printf "#   Max. Number of Inner Iteration     : %3d \n",maxInnerLoop + 1 > nIterOutPath
            printf "#   Max. Number of Solver Iteration    : %3d \n",maxSolverLoop > nIterOutPath
            printf "# \n" > nIterOutPath

            printf "variables= time nIter " > iniResOutPath
            printf "variables= time nIter " > finResOutPath
            printf "variables= time nIter " > nIterOutPath

            for (iter=0; iter < maxIter + 1; iter++)
            {
                printf "     iniRes%1d  ", iter+1 > iniResOutPath
                printf "     finRes%1d  ", iter+1 > finResOutPath
                printf "nSol%1d ", iter+1 > nIterOutPath
            }

            printf "\n" > iniResOutPath
            printf "\n" > finResOutPath
            printf "\n" > nIterOutPath

            printf "#           [-]  [-] " > iniResOutPath
            printf "#           [-]  [-] " > finResOutPath
            printf "#           [-]  [-] " > nIterOutPath

            for (iter=0; iter<maxIter+1;iter++)
            {
                printf "          [-] " > iniResOutPath
                printf "          [-] " > finResOutPath
                printf "  [-] " > nIterOutPath
            }

            printf "\n" > iniResOutPath
            printf "\n" > finResOutPath
            printf "\n" > nIterOutPath

            for (it = 0; it<iTime+1; it++)
            {
                printf "%15.7f %4d",Time[it],Niter[it]+1 > iniResOutPath
                printf "%15.7f %4d",Time[it],Niter[it]+1 > finResOutPath
                printf "%15.7f %4d",Time[it],Niter[it]+1 > nIterOutPath

                for (iter =0; iter < Niter[it]+1; iter++)
                {
                    printf " %13.6e", initialResidual[ivar,it,iter,iloop] > iniResOutPath
                    printf " %13.6e", finalResidual[ivar,it,iter,iloop] > finResOutPath
                    printf " %5d", solverIteration[ivar,it,iter,iloop] > nIterOutPath
                }

                for (iter = Niter[it]+1; iter<maxIter+1; iter++)
                {
                    printf " %13.6e", -1.0 > iniResOutPath
                    printf " %13.6e", -1.0 > finResOutPath
                    printf " %5d", -1  > nIterOutPath
                }

                printf "\n" > iniResOutPath
                printf "\n" > finResOutPath
                printf "\n" > nIterOutPath
            }
        }

    }
}
}
