BEGIN{
      iter = -1;
      iTime = -1;
      maxIter = 0;
     }
{
    if ($1=="Time" && $2=="=")
    {
        iTime++;
        Time[iTime] = $3;
    }

    if (iTime > -1)
    {
        if ($1=="PIMPLE:" && $2=="iteration")
        {
            iterPIMPLE = $3;
            if (iterPIMPLE=="1")
            {
                if (maxIter < iter)
                {
                    maxIter=iter
                }
                iter = 0;
            }
            else
            {
              iter++;
              Niter[iTime] = iter;
            }
        }

        if ($1=="time" && $2=="step" && $3=="continuity" && $4=="errors")
        {
            localSumError[iTime,iter]=$9;
            globalSumError[iTime,iter]=$12;
            cumulativeError[iTime,iter]=$15;
        }
    }
}
END{

    maxlocalError = 0;
    maxglobalSum = 0;
    maxCumulMassError = 0;

    for (it = 0; it<iTime+1; it++)
    {
        for (iter =0; iter < Niter[it]+1; iter++)
        {
            if (maxlocalError*maxlocalError < localSumError[it,iter]*localSumError[it,iter])
            {
                maxlocalError = localSumError[it,iter];
            }

            if (maxglobalSum*maxglobalSum < globalSumError[it,iter]*globalSumError[it,iter])
            {
                maxglobalSum = globalSumError[it,iter];
            }
        }
        if (maxCumulMassError*maxCumulMassError < cumulativeError[it,Niter[it]]*cumulativeError[it,Niter[it]])
        {
            maxCumulMassError = cumulativeError[it,Niter[it]];
        }
    }

    lastMassError = cumulativeError[iTime,Niter[iTime]];

    localSumOutPath = outFolder"/massError.localSum"
    globalSumOutPath = outFolder"/massError.globalSum"
    cumulativeOutPath = outFolder"/massError.cumulative"

    printf "# foamStar log extracted data \n#\n" > localSumOutPath
    printf "#   Data Type : Continuity Error \n" > localSumOutPath
    printf "#   Variable  : Local Sum \n#\n" > localSumOutPath
    printf "#   Max. Local Sum Error  : %13.6e \n", maxlocalError > localSumOutPath
    printf "# \n" > localSumOutPath

    printf "# foamStar log extracted data \n#\n" > globalSumOutPath
    printf "#   Data Type : Continuity Error \n" > globalSumOutPath
    printf "#   Variable  : Global Sum \n#\n" > globalSumOutPath
    printf "#   Max. Global Sum Error : %13.6e \n", maxglobalSum > globalSumOutPath
    printf "# \n" > globalSumOutPath

    printf "# foamStar log extracted data \n#\n" > cumulativeOutPath
    printf "#   Data Type : Courant Number \n" > cumulativeOutPath
    printf "#   Variable  : Cumulative \n#\n" > cumulativeOutPath
    printf "#   Max. cumulative Error : %13.6e \n", maxCumulMassError > cumulativeOutPath
    printf "#   Last cumulative Error : %13.6e \n", lastMassError > cumulativeOutPath
    printf "# \n" > cumulativeOutPath

    printf "variables= time Niter" > localSumOutPath
    printf "variables= time Niter" > globalSumOutPath

    printf "variables= time" > cumulativeOutPath

    for (iter=0; iter<maxIter+1;iter++)
    {
        printf "    localSum%1d ",iter+1 > localSumOutPath
        printf "   globalSum%1d ",iter+1 > globalSumOutPath
    }
    printf "  cumulativeError " > cumulativeOutPath

    printf "\n" > localSumOutPath
    printf "\n" > globalSumOutPath
    printf "\n" > cumulativeOutPath

	printf "#           [-]  [-]" > localSumOutPath
    printf "#           [-]  [-]" > globalSumOutPath
    printf "#           [-]" > cumulativeOutPath

    for (iter=0; iter<maxIter+1;iter++)
    {
        printf "           [-]" > localSumOutPath
        printf "           [-]" > globalSumOutPath
    }
    printf "           [-]" > cumulativeOutPath

    printf "\n" > localSumOutPath
    printf "\n" > globalSumOutPath
    printf "\n" > cumulativeOutPath

    for (it = 0; it<iTime+1; it++)
    {
        printf "%15.7f %4d",Time[it],Niter[it]+1 > localSumOutPath
        printf "%15.7f %4d",Time[it],Niter[it]+1 > globalSumOutPath
        printf "%15.7f",Time[it] > cumulativeOutPath

        for (iter =0; iter < Niter[it]+1; iter++)
        {
            printf " %13.6e", localSumError[it,iter] > localSumOutPath
            printf " %13.6e", globalSumError[it,iter] > globalSumOutPath
        }

        printf " %13.6e", cumulativeError[it,Niter[it]] > cumulativeOutPath

        for (iter = Niter[it]+1; iter<maxIter+1; iter++)
        {
            printf " %13.6e", 0.0 > localSumOutPath
            printf " %13.6e", 0.0 > globalSumOutPath
        }
        printf "\n" > localSumOutPath
        printf "\n" > globalSumOutPath
        printf "\n" > cumulativeOutPath
    }
}
