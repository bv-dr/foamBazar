#!/bin/bash
###
###  ecfoamTool : Extract Log bash script for foamStar
###
###  This script includes those functions
###
###     fsExtractLog [option] $logfile
###          : Extract whole information from $logfile
###
###     fsextractContinuityError [option] $logfile
###          : Extract continuity Error from $logfile
###
###     fsextractVOF [option] $logfile
###          : Extract volume of fraction from $logfile
###
###     fsextractCourant [option] $logfile
###          : Extract Courant Number from $logfile
###
###     fsextractResidual [option] $logfile
###          : Extract residual from $logfile
###
###     fsTestlogFileName [option] $logfile
###          : Test log file existence $logfile
###
###     This bash script needs sub-filder "extractFunction" which holds awk script
###     to extract information from log file.
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 24th Feb. 2017
###

setFoamStarLogExtract()
{
    fsLogExtractDir=$ftLogExtractDir

    ### foam Star Log Extract awk Folder (sub -function Folder)
    foamStarLogExtractSubFunctionDir="foamStarLogExtractFunction"

    ### sub function folder path
    foamStarLogExtractFunctionDir=$fsLogExtractDir/$foamStarLogExtractSubFunctionDir

    ### log output folder & default log file name
    foamStarLogOutFolder="logResult"
    defalutLogFileName="log.run"

    ### awk function name
    awk_file_VOFExtract="extractVOF.awk"
    awk_file_MassErrorExtract="extractContinuityError.awk"
    awk_file_CourantExtract="extractCourant.awk"
    awk_file_residualExtract="extractResidual.awk"

    awk_file_ResidualExtExtract="extractResidualExt.awk"

    awk_file_valueExtract="extractVariable.awk"

    ### awk function path
    awk_VOFExtract=$foamStarLogExtractFunctionDir/$awk_file_VOFExtract
    awk_MassErrorExtract=$foamStarLogExtractFunctionDir/$awk_file_MassErrorExtract
    awk_CourantExtract=$foamStarLogExtractFunctionDir/$awk_file_CourantExtract
    awk_residualExtract=$foamStarLogExtractFunctionDir/$awk_file_residualExtract

    awk_residualExtExtract=$foamStarLogExtractFunctionDir/$awk_file_ResidualExtExtract

    awk_valueExtract=$foamStarLogExtractFunctionDir/$awk_file_valueExtract
}

### function to test input log file
fsTestlogFileName()
{
    setFoamStarLogExtract

    inputLogFile=""

    if [ "$#" -lt 1 ]; then
        ### if no lf file is given, default log file name is used to extract log
        inputLogFile=$defalutLogFileName
        echo " "
        echo "     [WARNING] Log file is not given ! Log will be extracted for log.run"
        echo " "
    else
        if ! [ -d "$1" ]; then
            echo " "
            inputLogFile=$1
        else
            echo " "
            echo "    [ERROR] Directory is given to be extracted !! : $1"
            echo " "
        fi
    fi
}

### function to extract volume fraction from given log file
function fsextractValue()
{
    ### log file check
    fsTestlogFileName "$@"

    if [ -r "$inputLogFile" ]; then

        # Output Folder Check
        if [ ! -d "$foamStarLogOutFolder" ]; then
            mkdir "$foamStarLogOutFolder"
        else
            rm -rf $foamStarLogOutFolder/"massError.*"
        fi

        local outvarPath=$foamStarLogOutFolder/"value.txt"

        echo "     User Defined Value is      Extracting ..... "

        ### Call awk script file
        awk -f $awk_valueExtract \
               $inputLogFile \
            > $outvarPath
        echo "     ....................................... Done "
    else
        echo "[ERROR] No Log File : " "$inputLogFile"
    fi
}

### function to extract volume fraction from given log file
function fsextractVOF()
{
    ### log file check
    fsTestlogFileName "$@"

    if [ -r "$inputLogFile" ]; then

        # Output Folder Check
        if [ ! -d "$foamStarLogOutFolder" ]; then
            mkdir "$foamStarLogOutFolder"
        else
            rm -rf $foamStarLogOutFolder/"massError.*"
        fi

        echo "     Volume of Fraction Log File Extracting ..... "

        ### Call awk script file
        awk -voutFolder="${foamStarLogOutFolder}" \
            -f $awk_VOFExtract \
            $inputLogFile
        echo "     ....................................... Done "
    else
        echo "[ERROR] No Log File : " "$inputLogFile"
    fi
}

### function to continuity error from given log file
function fsextractContinuityError()
{
    ### log file check
    fsTestlogFileName "$@"

    if [ -r "$inputLogFile" ]; then

        # Output Folder Check
        if [ ! -d "$foamStarLogOutFolder" ]; then
            mkdir "$foamStarLogOutFolder"
        else
            rm -rf $foamStarLogOutFolder/"massError.*"
        fi

        echo "     Continuity Error Log File Extracting ....... "

        ### Call awk script file
        awk -voutFolder="${foamStarLogOutFolder}" \
            -f $awk_MassErrorExtract \
            $inputLogFile

        echo "     ....................................... Done "
    else
        echo "[ERROR] No Log File : " "$inputLogFile"
    fi
}

### function to extract Courant No. from given log file
function fsextractCourant()
{
    ### log file check
    fsTestlogFileName "$@"

    if [ -r "$inputLogFile" ]; then

        # Output Folder Check
        if [ ! -d "$foamStarLogOutFolder" ]; then
            mkdir "$foamStarLogOutFolder"
        else
            rm -rf $foamStarLogOutFolder/"CourantNumber"
        fi

        local courantOutpath=$foamStarLogOutFolder/"CourantNumber"

        ### Courant No. Extract ------------------------------------------------------
        echo "     Courant No. Log File Extracting ............ "

        ### Call awk script file
        awk -f $awk_CourantExtract \
            $inputLogFile \
            > $courantOutpath

            echo "     ....................................... Done "

        ### Courant No. Extract ------------------------------------------------------
    else
        echo "[ERROR] No Log File : " "$inputLogFile"
    fi
}

### function to extract Residual from given log file
###
###   The defined variables for the residual is given in awk file
###
function fsextractResidual()
{
    ### log file check
    fsTestlogFileName "$@"

    if [ -r "$inputLogFile" ]; then

        # Output Folder Check
        if [ ! -d "$foamStarLogOutFolder" ]; then
            mkdir "$foamStarLogOutFolder"
        else
            rm -rf $foamStarLogOutFolder/"initialResidual.*"
            rm -rf $foamStarLogOutFolder/"finalResidual.*"
        fi

        echo "     Residual Log File Extracting ............... "

        ### Call awk script file
        awk -voutFolder="${foamStarLogOutFolder}" \
            -f $awk_residualExtract \
            $inputLogFile

        echo "     ....................................... Done "

    else
        echo "[ERROR] No Log File : " "$inputLogFile"
    fi

}

### function to extract Residual from given log file
###
###   The defined variables for the residual is given in awk file
###
function fsextractResidualExt()
{
    ### log file check
    fsTestlogFileName "$@"

    if [ -r "$inputLogFile" ]; then

        # Output Folder Check
        if [ ! -d "$foamStarLogOutFolder" ]; then
            mkdir "$foamStarLogOutFolder"
        else
            rm -rf $foamStarLogOutFolder/"resInit.*"
            rm -rf $foamStarLogOutFolder/"resAbs.*"
            rm -rf $foamStarLogOutFolder/"resPrev.*"
            rm -rf $foamStarLogOutFolder/"resRelPrev.*"
        fi

        echo "     External Residual Log File Extracting ....... "

        ### Call awk script file
        awk -voutFolder="${foamStarLogOutFolder}" \
            -f $awk_residualExtExtract \
            $inputLogFile

        echo "     ....................................... Done "

    else
        echo "[ERROR] No Log File : " "$inputLogFile"
    fi

}

### function to whole information from given log file
function fsExtractLog()
{
    ### log file check
    fsTestlogFileName "$@"

    if [ -r "$inputLogFile" ]; then

        ### bash function files to extract log
        echo "  foamStar Log Data Extract : " "$logFile"

        # Output Folder Check
        if [ ! -d "$foamStarLogOutFolder" ]; then
            mkdir "$foamStarLogOutFolder"
        else
            rm -rf $foamStarLogOutFolder/*
        fi

        ### Courant No. Extract
        fsextractCourant $inputLogFile

        ### Continuity Error Extract
        fsextractContinuityError $inputLogFile

        ### VOF Extract
        fsextractVOF $inputLogFile

        ### Residual Extract
        fsextractResidual $inputLogFile

    else
        echo "[ERROR] No Log File : " "$inputLogFile"
    fi

    echo " "
}
