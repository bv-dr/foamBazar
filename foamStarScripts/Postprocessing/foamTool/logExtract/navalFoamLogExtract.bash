#!/bin/bash
###
###  ecfoamTool : Extract Log bash script for navalFoam
###
###  This script includes those functions
###
###     nfExtractLog [option] $logfile
###          : Extract whole information from $logfile
###
###     nfextractContinuityError [option] $logfile
###          : Extract continuity Error from $logfile
###
###     nfextractVOF [option] $logfile
###          : Extract volume of fraction from $logfile
###
###     nfextractCourant [option] $logfile
###          : Extract Courant Number from $logfile
###
###     nfextractResidual [option] $logfile
###          : Extract residual from $logfile
###
###     nfTestlogFileName [option] $logfile
###          : Test log file existence $logfile
###
###     This bash script needs sub-filder "extractFunction" which holds awk script
###     to extract information from log file.
###
###     Author : YoungMyung Choi, youngmyung.choi@ec-nantes.fr
###     Date   : 24th Feb. 2017
###
setNavalFoamLogExtract()
{
    nfLogExtractDir=$ftLogExtractDir

    ### foam Star Log Extract awk Folder (sub -function Folder)
    navalFoamLogExtractSubFunctionDir="navalFoamExtractFunction"

    ### sub function folder path
    navalFoamExtractFunctionDir=$nfLogExtractDir/$navalFoamLogExtractSubFunctionDir

    ### log output folder & default log file name
    navalFoamLogOutFolder="logResult"
    defalutLogFileName="log.navalFoam"

    ### awk function name
    awk_file_VOFExtract="extractVOF.awk"
    awk_file_MassErrorExtract="extractContinuityError.awk"
    awk_file_CourantExtract="extractCourant.awk"
    awk_file_residualExtract="extractResidual.awk"

    ### awk function path
    awk_VOFExtract=$navalFoamExtractFunctionDir/$awk_file_VOFExtract
    awk_MassErrorExtract=$navalFoamExtractFunctionDir/$awk_file_MassErrorExtract
    awk_CourantExtract=$navalFoamExtractFunctionDir/$awk_file_CourantExtract
    awk_residualExtract=$navalFoamExtractFunctionDir/$awk_file_residualExtract
}

### function to test input log file
nfTestlogFileName()
{
    setNavalFoamLogExtract
    
    inputLogFile=""

    if [ "$#" -lt 1 ]; then
        ### if no lf file is given, default log file name is used to extract log
        inputLogFile=$defalutLogFileName
        echo " "
        echo "     [WARNING] Log file is not given ! Log will be extracted for $defalutLogFileName"
        echo " "nfTestlogFileName
    else
        if ! [ -d "$1" ]; then
            echo " "
            inputLogFile=$1
        else
            echo " "
            echo "    [ERROR] Directory is given to be extracted !! : $1"
            echo " "
            exit 1
        fi
    fi
}

### function to extract volume fraction from given log file
function nfextractVOF()
{
    ### log file check
    nfTestlogFileName "$@"

    if [ -r "$inputLogFile" ]; then

        # Output Folder Check
        if [ ! -d "$navalFoamLogOutFolder" ]; then
            mkdir "$navalFoamLogOutFolder"
        else
            rm -rf $navalFoamLogOutFolder/"massError.*"
        fi

        echo "     Volume of Fraction Log File Extracting ..... "

        ### Call awk script file
        awk -voutFolder="${navalFoamLogOutFolder}" \
            -f $awk_VOFExtract \
            $inputLogFile
        echo "     ....................................... Done "
    else
        echo "[ERROR] No Log File : " "$fileName"
    fi
}

### function to continuity error from given log file
function nfextractContinuityError()
{
    ### log file check
    nfTestlogFileName "$@"

    if [ -r "$inputLogFile" ]; then

        # Output Folder Check
        if [ ! -d "$navalFoamLogOutFolder" ]; then
            mkdir "$navalFoamLogOutFolder"
        else
            rm -rf $navalFoamLogOutFolder/"massError.*"
        fi

        echo "     Continuity Error Log File Extracting ....... "

        ### Call awk script file
        awk -voutFolder="${navalFoamLogOutFolder}" \
            -f $awk_MassErrorExtract \
            $inputLogFile

        echo "     ....................................... Done "
    else
        echo "[ERROR] No Log File : " "$fileName"
    fi
}

### function to extract Courant No. from given log file
function nfextractCourant()
{
    ### log file check
    nfTestlogFileName "$@"

    if [ -r "$inputLogFile" ]; then

        # Output Folder Check
        if [ ! -d "$navalFoamLogOutFolder" ]; then
            mkdir "$navalFoamLogOutFolder"
        else
            rm -rf $navalFoamLogOutFolder/"CourantNumber"
        fi

        local courantOutpath=$navalFoamLogOutFolder/"CourantNumber"

        ### Courant No. Extract ------------------------------------------------------
        echo "     Courant No. Log File Extracting ............ "

        ### Call awk script file
        awk -f $awk_CourantExtract \
            $inputLogFile \
            > $courantOutpath

            echo "     ....................................... Done "

        ### Courant No. Extract ------------------------------------------------------
    else
        echo "[ERROR] No Log File : " "$fileName"
    fi
}

### function to extract Residual from given log file
###
###   The defined variables for the residual is given in awk file
###
function nfextractResidual()
{
    ### log file check
    nfTestlogFileName "$@"

    if [ -r "$inputLogFile" ]; then

        # Output Folder Check
        if [ ! -d "$navalFoamLogOutFolder" ]; then
            mkdir "$navalFoamLogOutFolder"
        else
            rm -rf $navalFoamLogOutFolder/"initialResidual.*"
            rm -rf $navalFoamLogOutFolder/"finalResidual.*"
        fi

        echo "     Residual Log File Extracting ............... "

        ### Call awk script file
        awk -voutFolder="${navalFoamLogOutFolder}" \
            -f $awk_residualExtract \
            $inputLogFile

        echo "     ....................................... Done "

    else
        echo "[ERROR] No Log File : " "$fileName"
    fi

}

### function to whole information from given log file
function nfExtractLog()
{
    ### log file check
    nfTestlogFileName "$@"

    if [ -r "$inputLogFile" ]; then

        ### bash function files to extract log
        echo "  navalFoam Log Data Extract : " "$logFile"

        # Output Folder Check
        if [ ! -d "$navalFoamLogOutFolder" ]; then
            mkdir "$navalFoamLogOutFolder"
        else
            rm -rf $navalFoamLogOutFolder/*
        fi

        ### Courant No. Extract
        nfextractCourant $inputLogFile

        ### Continuity Error Extract
        nfextractContinuityError $inputLogFile

        ### VOF Extract
        nfextractVOF $inputLogFile

        ### Residual Extract
        nfextractResidual $inputLogFile

    else
        echo "[ERROR] No Log File : " "$fileName"
    fi

    echo " "
}
