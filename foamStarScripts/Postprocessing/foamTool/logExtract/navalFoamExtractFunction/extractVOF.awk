BEGIN{
      iter = -1;
      iTime = -1;
      maxIter = 0;
}
{
    if ($1=="Time" && $2=="=")
    {
        iTime++;
        Time[iTime] = $3;

        if (maxIter < iter)
        {
            maxIter=iter
        }
        iter = -1
    }

    if (iTime > -1)
    {
        if ($2=="Solving" && $3=="for" && $4 =="Ux,")
        {
            iter++
            Niter[iTime] = iter;
        }

        if ($1=="Liquid" && $2=="phase" && $3=="volume" && $4 == "fraction")
        {
            VOF[iTime,iter]=$6
            VOFMin[iTime,iter]=$9
            VOFMax[iTime,iter]=$12
        }
    }
}
END{
    VOFSimpleOutPath = outFolder"/VOF.simple"

    VOFOutPath       = outFolder"/VOF.vof"
    VOFMinOutPath    = outFolder"/VOF.min"
    VOFMaxOutPath    = outFolder"/VOF.max"

    minVOF =  1e6;
    maxVOF = -1e6;

    for (it = 0; it<iTime+1; it++)
    {
        for (iter =0; iter < Niter[it]+1; iter++)
        {
            if (minVOF < VOFMin[it,iter])
            {
                minVOF = VOFMin[it,iter];
            }

            if (maxVOF > VOFMax[it,iter])
            {
                maxVOF = VOFMax[it,iter];
            }
        }
    }

    printf "variables= time niter" > VOFSimpleOutPath
    printf "variables= time niter" > VOFOutPath
    printf "variables= time niter" > VOFMinOutPath
    printf "variables= time niter " > VOFMaxOutPath


    printf "       VOFMin        VOFMax           VOF" > VOFSimpleOutPath

    for (iter=0; iter<maxIter+1;iter++)
    {fb
        printf "      VOFMin%1d ",iter+1 > VOFMinOutPath
        printf "      VOFMax%1d ",iter+1 > VOFMaxOutPath
        printf "         VOF%1d ",iter+1 > VOFOutPath
    }

    printf "\n" > VOFSimpleOutPath
    printf "\n" > VOFOutPath
    printf "\n" > VOFMinOutPath
    printf "\n" > VOFMaxOutPath

    printf "#           [-]  [-] " > VOFSimpleOutPath
    printf "#           [-]  [-] " > VOFOutPath
    printf "#           [-]  [-] " > VOFMinOutPath
    printf "#           [-]  [-] " > VOFMaxOutPath

    printf "          [-]           [-]           [-]     " > VOFSimpleOutPath

    for (iter=0; iter<maxIter+1;iter++)
    {
        printf "          [-] " > VOFOutPath
        printf "          [-] " > VOFMinOutPath
        printf "          [-] " > VOFMaxOutPath
    }

    printf "\n" > VOFSimpleOutPath
    printf "\n" > VOFOutPath
    printf "\n" > VOFMinOutPath
    printf "\n" > VOFMaxOutPath

    for (it = 0; it<iTime; it++)
    {
        printf "%15.7f %4d",Time[it],Niter[it]+1 > VOFSimpleOutPath
        printf "%15.7f %4d",Time[it],Niter[it]+1 > VOFOutPath
        printf "%15.7f %4d",Time[it],Niter[it]+1 > VOFMinOutPath
        printf "%15.7f %4d",Time[it],Niter[it]+1 > VOFMaxOutPath

        for (iter =0; iter < Niter[it]+1; iter++)
        {
            printf " %13.6e",VOF[it,iter] > VOFOutPath
            printf " %13.6e",VOFMin[it,iter] > VOFMinOutPath
            printf " %13.6e",VOFMax[it,iter] > VOFMaxOutPath
        }

        for (iter = Niter[it]+1; iter<maxIter+1; iter++)
        {
            printf " %13.6e", 0.0 > VOFOutPath
            printf " %13.6e", 0.0 > VOFMinOutPath
            printf " %13.6e", 0.0 > VOFMinOutPath
        }

        printf " %13.6e %13.6e %13.6e",VOFMin[it,Niter[it]],VOFMax[it,Niter[it]],VOF[it,Niter[it]] > VOFSimpleOutPath

        printf "\n" > VOFSimpleOutPath
        printf "\n" > VOFOutPath
        printf "\n" > VOFMinOutPath
        printf "\n" > VOFMaxOutPath

    }
}
