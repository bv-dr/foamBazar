#!/bin/bash -l
# The -l above is required to get the full environment with modules

# Set the allocation to be charged for this job
# not required if you have set a default allocation
#SBATCH -A 202X-X-XX

# The name of the script is myjob
#SBATCH -J rec_name

# Only 1 hour wall-clock time will be given to this job
#SBATCH -t 9-00:00:00

# Number of nodes
#SBATCH -N 1

# Number of MPI processes per node (the following is actually the default)
#SBATCH -n 4
#SBATCH --account=
#SBATCH --qos=

# Number of MPI processes.

#SBATCH -e reconstruct_error_file.e
#SBATCH -o reconstruct_output_file.o

reconstructPar -time '2:'
