#!/bin/bash -l
# The -l above is required to get the full environment with modules

# Set the allocation to be charged for this job
# not required if you have set a default allocation
#SBATCH -A 202X-X-XX

# The name of the script is myjob
#SBATCH -J run_name

# Only 1 hour wall-clock time will be given to this job
#SBATCH -t 9-00:00:00

# Number of nodes
#SBATCH -N 3

# Number of MPI processes per node (the following is actually the default)
#SBATCH -n 64
#SBATCH --account=
#SBATCH --qos=

# Number of MPI processes.

#SBATCH -e error_file.e
#SBATCH -o output_file.o


foam_mpirun -np 64 foamStarFwdvel -parallel
