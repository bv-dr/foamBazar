#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 08:31:00 2021

@author: theodescamps
"""

import os.path as pth
import os
import sys
import collections


#def checkFolder(path):
#    needs=['bodyDict','boundaryCondtitions']

def writeHead(file,location,name):
    head  = "/*--------------------------------*- C++ -*----------------------------------*\ \n"
    head += "=========                 | \n"
    head += "\\\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox \n"
    head += " \\\    /   O peration     | Website:  https://openfoam.org \n"
    head += "  \\\  /    A nd           | Version:  8 \n"
    head += "   \\\/     M anipulation  | \n"
    head += "\*---------------------------------------------------------------------------*/ \n"
#    head += "FoamFile \n"
#    head += "{ \n"
#    head += "\tversion     2.0; \n"
#    head += "\tformat      ascii; \n"
#    head += "\tclass       dictionary; \n"
#    head += '\tlocation    "'+location+'"; \n'
#    head += '\tobject      '+name+'; \n'
#    head += "} \n"
#    head += "// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * // \n\n\n"
    
    file.write(head)
    
def cleanComment(buff):
    
    sl=[]
    el=[]
    
    skip = 0
    comment = False
    n = len(buff)
    k=0
    while k < n-1:
        
        if comment:
            if buff[k]=='\n':
                if skip == 0:
                    el.append(k)
                comment = False
        else:
            if buff[k:k+2]=='//':
                if skip == 0:
                    sl.append(k)
                comment = True
                k+=1
                
            elif buff[k:k+2]=='/*':
                if skip == 0:
                    sl.append(k)
                skip += 1
                k+=1
                
            elif buff[k:k+2]=='*/':
                skip -= 1
                if skip == 0:
                    el.append(k+2)
                if skip < 0:
                    print ("ERROR ! Unexpected '*/' was found")
                    return
                k+=1
        k+=1
        
    
    if len(sl) == 0:
        return buff
    
    else:
        buffc = buff[:sl[0]]
        for i in range(len(sl)-1):
            buffc+=buff[el[i]:sl[i+1]]
        
        if len(el) == len(sl):
            buffc+=buff[el[-1]:]
        
    return buffc

def cleanVal(val):
    valc = ""
    last = False
    for c in val:
        if c =='\n' or c =='\t' or c ==' ':
            if not last:
                last=True
                valc+=' '
        else:
            last=False
            valc+=c
    return valc

def readDict(buff, dico, path):
    indict = 0

    n=len(buff)
    
    k, sl =0, 0
    
    key = ""
    val = ""
    actDict = [dico]
    waitKey = True
    inKey = False
    
    bracket = 0
    
    while k<n:
        
        if buff[k] == '(':
            bracket+=1
            if waitKey:
                print ("ERROR ! Unexpected '(' was found. Waiting for Key")
                return
            
        elif buff[k] == ')':
            bracket-=1
            if waitKey:
                print ("ERROR ! Unexpected ')' was found. Waiting for Key")
                return
            
            if bracket < 0:
                print ("ERROR ! Unexpected ')' was found")
                return
            
        elif bracket == 0:
            if buff[k] == '{':
                if inKey:
                    key = buff[sl:k].strip()
                    sl=k
                    inKey = False
                    
                waitKey = True
                
                if len(key) == 0:
                    print ("ERROR ! Unamed subDict")
                    return
                
                indict+=1
                actDict[-1][key]=collections.OrderedDict()
                actDict.append(actDict[-1][key])
                
                sl=k+1
                
            elif buff[k] == '}':
                indict-=1
                if indict < 0:
                    print ("ERROR ! Unexpected '}' was found")
                    return
                
                actDict.pop()
                waitKey = True
                key =""
                sl=k+1
            
            elif (buff[k] == " " or buff[k] == "\t" or buff[k] == "\n") and inKey:
                key = buff[sl:k].strip()
                sl=k+1
                inKey = False
                actDict[-1][key]=''
                if len(key) == 0:
                    print ("ERROR ! Empty key was found")
                    return
                elif key[0]=='$':
                    waitKey = True
                    key = ""
                
            elif buff[k] == ';' or (buff[k] == "\n" and key == '#include'): #Write key; as {key:""}
                if inKey:
                    key = buff[sl:k].strip()
                    sl=k
                    inKey = False
                
                if len(key) != 0:
                    val = cleanVal(buff[sl:k].strip())
                    actDict[-1][key]=val
                waitKey = True
                key = ""
            
            elif waitKey and buff[k] != " " and buff[k] != "\t" and buff[k] != "\n":
                waitKey = False
                inKey = True
                sl=k
            
        k+=1
    if bracket != 0:
        print ("ERROR ! '( )' syntax error in file: ", path)
        return
    if indict != 0:
        print ("ERROR ! '{ }' syntax error in file: ", path)
        return
        
def importDict(dirPath,location,name):
    path = pth.join(dirPath,location)
    path = pth.join(path,name)
    
    f = open(path,'r')
    if not f:
        print ("ERROR oppenning file: ", path)
        return
    
    buff = cleanComment(f.read())
    dico = collections.OrderedDict()
    readDict(buff, dico, path)
    return  dico


def writeDict(dirPath,location,name, dico):
    path = pth.join(dirPath,location)
    path = pth.join(path,name)
    
    file = open(path,'w')
    if not file:
        print ("ERROR oppenning file: ", path)
        return
    
    
    lenMaxKey = 40
    
    writeHead(file,location,name)

    actDict=[dico]
    actPos=[0]
    
    nbTab = 0 
    nb=0
    nbMax = 1000
    dictKey = ""
    while len(actDict) > 0 and nb < nbMax :
        while actPos[-1] < len(actDict[-1])  and nb < nbMax :
            nb+=1
            keyList=[]
            for i in actDict[-1].keys():
                keyList.append(i)
                
            key = keyList[actPos[-1]]
            value = actDict[-1][key]
            
            if type(value) is collections.OrderedDict:
                dictKey = key
                actPos[-1]+=1
                actDict.append(value)
                actPos.append(0)
                line = '\n'+'\t'*nbTab + key + '\n' + '\t'*nbTab + '{\n'
                file.write(line)
                nbTab+=1
                
                
            elif len(key) > 0 and key[0]=='$':
                line = '\t'*nbTab + key + '\n'
                
                file.write(line)
                actPos[-1]+=1
            elif len(value)>0:
                
                line = '\t'*nbTab + key + ' '*(lenMaxKey-len(key)) + value + ';\n'
                
                if key == "#include":
                    line = '\t'*nbTab + key + ' '*(lenMaxKey-len(key)) + value + '\n'
                
                file.write(line)
                actPos[-1]+=1
            else:
                line = '\t'*nbTab + key + ';\n'
                
                file.write(line)
                actPos[-1]+=1
                
        nbTab-=1
        
            
        actPos.pop()
        actDict.pop()
        
        if len(actDict) > 0:
            if dictKey == "FoamFile":
                line ='\t'*nbTab + '}\n'
                line += "// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * // \n\n\n"
            else:
                line ='\t'*nbTab + '}\n\n'
            file.write(line)
        
    if nb==nbMax:
        print('ERROR ! Maximal number of entry reached !')
    
def addDict2Dict(source, target):
    
    actTaget = [target]
    actDict = [source]
    actPos=[0]
    path =['']
    
    nb=0
    nbMax = 1000
    
    while len(actDict) > 0 and nb < nbMax :
        while actPos[-1] < len(actDict[-1])  and nb < nbMax :
            nb+=1
            keyList=[]
            for i in actDict[-1].keys():
                keyList.append(i)
                
            key = keyList[actPos[-1]]
            value = actDict[-1][key]
            
            if type(value) is collections.OrderedDict:
                
                if not(key in actTaget[-1]):
                    actTaget[-1][key]=collections.OrderedDict()
                    print ('INFO: New entry "',key,'{} "  was added to the dictionary "',path[-1],'" !')
                            
                actPos[-1]+=1
                actDict.append(value)
                actPos.append(0)
                if len(path) > 1:
                    path.append(path[-1]+'->'+key)
                else:
                    path.append(path[-1]+key)
                actTaget.append(actTaget[-1][key])
                
                
            else:
                if not(key in actTaget[-1]):
                    print ('INFO: New entry "',key,' = ',value,'" was added to the dictionary "',path[-1],'" !')
                else:
                    print ('INFO: The entry "',key,' = ',actTaget[-1][key],'" was modified by "',key,' = ',value,'" in the dictionary "',path[-1],'" !')
                
                actTaget[-1][key] = value
                actPos[-1]+=1

        
        actTaget.pop()
        actPos.pop()
        actDict.pop()
        path.pop()
        
    if nb==nbMax:
        print('ERROR ! Maximal number of entry reached !')
    
    
    
        
def writeFolder(dirPath, folderDict):
    
    for key, value in folderDict.items():
        path = pth.join(dirPath,key)
        
        if not pth.exists(pth.dirname(path)):
            cmd = "mkdir -p " + pth.dirname(path)
            os.system(cmd)
        
        writeDict(dirPath, pth.dirname(key), pth.basename(key), value)
        

def getV(dico,key,default):
    if ('key' in dico):
        return dico[key]
    else:
        return default


def addPatches(init, toAdd):
    initStrip = init.strip()
    addStrip = toAdd.strip()
    tab1 = []
    tab2 = []
    if len(initStrip) > 0:
        tab1=initStrip[1:-1].split()
    if len(addStrip) > 0:
        tab2=addStrip[1:-1].split()
    final="("
    for x in tab1:
        final += x + ' '
    for x in tab2:
        if not(x in tab1):
            final += x + ' '
    final+=")"
        
    return final

def getPatches(patches):
    patchesStrip = patches.strip()
    tab = []
    if len(patchesStrip) > 0:
        tab=patchesStrip[1:-1].split()
    return tab

def nbPatches(patches):
    patchesStrip = patches.strip()
    tab = []
    if len(patchesStrip) > 0:
        tab=patchesStrip[1:-1].split()
    return len(tab)

def patch2regEx(patches):
    patchtab = getPatches(patches)
    
    if len(patchtab) > 1:
        key = '"('
        for p in patchtab[:-1]:
            key+=p+'|'
        
        key+=patchtab[-1]
        key += ')"'
        return key
    elif len(patchtab) > 0:
        return patchtab[0]
    else:
        return ''

def switch(p):
    return (p.lower() == 'true' or p.lower() == 'yes')








###################################################################################
#########                          START MAIN                             #########
###################################################################################

def main(refDirPath,bvsDirPath):
    
    ################### IMPORT BVS DICTS ###################
    
    bvsWraper = collections.OrderedDict()
    
    bvsWraper['bodyDict'] = importDict(bvsDirPath,'','bodyDict')
    bvsWraper['boundaryConditionsDict'] = importDict(bvsDirPath,'','boundaryConditionsDict')
    bvsWraper['paramDict'] = importDict(bvsDirPath,'','paramDict')
    bvsWraper['optionDict'] = importDict(bvsDirPath,'','optionDict')
    bvsWraper['physicsDict'] = importDict(bvsDirPath,'','physicsDict')
    
    
    ################### IMPORT REF DICTS ###################
    
    foamStarDict = collections.OrderedDict()
    
    foamStarDict['constant/dynamicMeshDict'] = importDict(refDirPath,'constant','dynamicMeshDict')
    foamStarDict['constant/fvOptions'] = importDict(refDirPath,'constant','fvOptions')
    foamStarDict['constant/g'] = importDict(refDirPath,'constant','g')
    foamStarDict['constant/transportProperties'] = importDict(refDirPath,'constant','transportProperties')
    foamStarDict['constant/turbulenceProperties'] = importDict(refDirPath,'constant','turbulenceProperties')
    foamStarDict['constant/waveProperties'] = importDict(refDirPath,'constant','waveProperties')
    
    foamStarDict['system/controlDict'] = importDict(refDirPath,'system','controlDict')
    foamStarDict['system/decomposeParDict'] = importDict(refDirPath,'system','decomposeParDict')
    foamStarDict['system/fvSchemes'] = importDict(refDirPath,'system','fvSchemes')
    foamStarDict['system/fvSolution'] = importDict(refDirPath,'system','fvSolution')
    
    ################### PRELIMINARY CHECK ###################
    simulationType = "Multiphase/Ship"
    if "simulationType" in bvsWraper['boundaryConditionsDict']:
        simulationType = bvsWraper['boundaryConditionsDict']["simulationType"]
    
    if simulationType == "OpenWater":
        foamStarDict['system/setFieldsDict'] = importDict(refDirPath,'system/openWater','setFieldsDict')
        
    symmetric = False
    if 'symmetryPatch' in bvsWraper['boundaryConditionsDict']:
        if nbPatches(bvsWraper['boundaryConditionsDict']['symmetryPatch']) > 0:
            symmetric = True
    
    ################### 0/org ###################
        
    boundaryTypePath = pth.join('howToDef/bondaryConditions',simulationType)
    
    listOfFile = ['alpha.water','p_rgh','U','nut','k','omega']
    if ('simulationType' in bvsWraper['physicsDict']) and bvsWraper['physicsDict']['simulationType'] == 'laminar':
        listOfFile = ['alpha.water','p_rgh','U']
    
    for f in listOfFile:
        name = '0/org/'+f
        tmpDict = importDict(refDirPath,boundaryTypePath,f)
        foamStarDict[name]=collections.OrderedDict()
        foamStarDict[name]['FoamFile']=tmpDict['FoamFile']
        foamStarDict[name]['dimensions']=tmpDict['dimensions']
        foamStarDict[name]['internalField']=tmpDict['internalField']
        foamStarDict[name]['boundaryField']=collections.OrderedDict()
        
        
        if 'inletPatch' in bvsWraper['boundaryConditionsDict']:
            key = patch2regEx(bvsWraper['boundaryConditionsDict']['inletPatch'])
            if key != '':
                foamStarDict[name]['boundaryField'][key]=tmpDict['boundaryField']['inlet']
        
        if 'outletPatch' in bvsWraper['boundaryConditionsDict']:
            key = patch2regEx(bvsWraper['boundaryConditionsDict']['outletPatch'])
            if key != '':
                foamStarDict[name]['boundaryField'][key]=tmpDict['boundaryField']['outlet']
            
        if 'sidePatch' in bvsWraper['boundaryConditionsDict']:
            key = patch2regEx(bvsWraper['boundaryConditionsDict']['sidePatch'])
            if key != '':
                foamStarDict[name]['boundaryField'][key]=tmpDict['boundaryField']['side']
    
        if 'topPatch' in bvsWraper['boundaryConditionsDict']:
            key = patch2regEx(bvsWraper['boundaryConditionsDict']['topPatch'])
            if key != '':
                foamStarDict[name]['boundaryField'][key]=tmpDict['boundaryField']['top']
            
        if 'bottomPatch' in bvsWraper['boundaryConditionsDict']:
            key = patch2regEx(bvsWraper['boundaryConditionsDict']['bottomPatch'])
            if key != '':
                foamStarDict[name]['boundaryField'][key]=tmpDict['boundaryField']['bottom']
            
        if 'symmetryPatch' in bvsWraper['boundaryConditionsDict']:
            if simulationType.lower().rfind('2d') != -1:
                print('ERROR ! SymmetricPatch cannot be defined for 2D cases !')
            
            key = patch2regEx(bvsWraper['boundaryConditionsDict']['symmetryPatch'])
            if key != '':
                foamStarDict[name]['boundaryField'][key]=tmpDict['boundaryField']['symmetry']
            
        if 'wallPatch' in bvsWraper['boundaryConditionsDict']:
            key = patch2regEx(bvsWraper['boundaryConditionsDict']['wallPatch'])
            if key != '':
                foamStarDict[name]['boundaryField'][key]=tmpDict['boundaryField']['wall']
            
    ################### constant/dynamicMeshDict #########################
    
    bodyList = []
    bodyPatches = []
    
    workingDict = foamStarDict['constant/dynamicMeshDict']['multiBodyFvMeshCoeffs']
    bodyFound = False
    for key, value in bvsWraper['bodyDict'].items():
        if type(value) is collections.OrderedDict:
            if ('type' in value) and value['type'] == 'rigidBody':
                bodyFound = True
                bodyList.append(key)
                bodydata=value
                
                bodyRef=importDict(refDirPath,'howToDef','body')
                workingDict[key] = bodyRef['bodyName'].copy()
                
                if 'bodyPatch' in bodydata:
                    workingDict['hullPatches']=addPatches(workingDict['hullPatches'],bodydata['bodyPatch'])
                    workingDict['motionPatches']=addPatches(workingDict['motionPatches'],bodydata['bodyPatch'])
                    workingDict[key]['loads']['fluid']['patches'] = bodydata['bodyPatch']
                    bodyPatches.append(bodydata['bodyPatch'])

                    
                if symmetric:
                    workingDict[key]['loads']['fluid']['ySym'] = 'true'
                else:
                    workingDict[key]['loads']['fluid']['ySym'] = 'false'
                
                
                if 'mass' in bodydata:
                    workingDict[key]['mass']=bodydata['mass']
                if 'momentOfInertia' in bodydata:
                    workingDict[key]['momentOfInertia']=bodydata['momentOfInertia']
                if 'centerOfGravity' in bodydata:
                    workingDict[key]['CoRInitial']=bodydata['centerOfGravity']
                    
                if 'Lpp' in bodydata:
                    workingDict[key]['loads']['fsBodyfExtDamping']['Lpp']=bodydata['Lpp']
                if 'Breadth' in bodydata:
                    workingDict[key]['loads']['fsBodyfExtDamping']['Breadth']=bodydata['Breadth']
                if 'DoFs' in bodydata:
                    workingDict[key]['constraints']['userDefineDofs']['except']=bodydata['DoFs']
                if 'heaveDampingCoef' in bodydata:
                    workingDict[key]['loads']['fsBodyfExtDamping']['heaveDampingCoef']=bodydata['heaveDampingCoef']
                if 'pitchDampingCoef' in bodydata:
                    workingDict[key]['loads']['fsBodyfExtDamping']['pitchDampingCoef']=bodydata['pitchDampingCoef']
                
                if 'gravity' in bvsWraper['physicsDict']:
                    line = '(0 0 -'+bvsWraper['physicsDict']['gravity']+')'
                    workingDict[key]['loads']['gravity']['value'] = line
    
    if not bodyFound:
        foamStarDict['constant/dynamicMeshDict']['dynamicFvMesh']='staticFvMesh'
        foamStarDict['constant/dynamicMeshDict'].pop('multiBodyFvMeshCoeffs')
    
    
    ################### constant/fvOptions #########################
    
    for key, value in bvsWraper['optionDict'].items():
        foamStarDict['constant/fvOptions'][key] = value
    
    ################### constant/g #########################
    
    if 'gravity' in bvsWraper['physicsDict']:
        line = '(0 0 -'+bvsWraper['physicsDict']['gravity']+')'
        foamStarDict['constant/g']['value'] = line
    
    ################### constant/transportProperties #########################
    
    if 'multiphase' in bvsWraper['physicsDict'] and (bvsWraper['physicsDict']['multiphase'].lower() == ('false' or 'no')):
        print ("coucou")
    
    else:
        
        if 'rhoWater' in bvsWraper['physicsDict']:
            line = 'rho [1 -3 0 0 0 0 0] ' + bvsWraper['physicsDict']['rhoWater']
            foamStarDict['constant/transportProperties']['"phase1|water"']['rho'] = line
        if 'nuWater' in bvsWraper['physicsDict']:
            line = 'nu [0 2 -1 0 0 0 0] ' + bvsWraper['physicsDict']['nuWater']
            foamStarDict['constant/transportProperties']['"phase1|water"']['nu'] = line
            
        if 'rhoAir' in bvsWraper['physicsDict']:
            line = 'rho [1 -3 0 0 0 0 0] ' + bvsWraper['physicsDict']['rhoAir']
            foamStarDict['constant/transportProperties']['"phase2|air"']['rho'] = line
        if 'nuAir' in bvsWraper['physicsDict']:
            line = 'nu [0 2 -1 0 0 0 0] ' + bvsWraper['physicsDict']['nuAir']
            foamStarDict['constant/transportProperties']['"phase2|air"']['nu'] = line
        
            
        
    ################### constant/turbulenceProperties #########################
    
    if 'simulationType' in bvsWraper['physicsDict']:
        foamStarDict['constant/turbulenceProperties']['simulationType'] = bvsWraper['physicsDict']['simulationType']
    
    ################### constant/waveProperties #########################
    workingDict = foamStarDict['constant/waveProperties']
    
    patches = '()'
    
    if 'waves' in bvsWraper['physicsDict']:
        patches = '()'
        farfieldDistances = '()'
        blendingDistances = '()'
        
        workingDict['farfieldCoeffs'] = bvsWraper['physicsDict']['waves'].copy()
        
        if workingDict['farfieldCoeffs']['waveType']=='HOS' and not('Grid2GridInputFile' in workingDict['farfieldCoeffs']):
            workingDict['farfieldCoeffs']['Grid2GridInputFile'] =  '../Grid2Grid.dict'
    else:
        workingDict['farfieldCoeffs'] = collections.OrderedDict()
        workingDict['farfieldCoeffs']['waveType'] = 'noWaves'
        workingDict['farfieldCoeffs']['startTime'] = '0'
        workingDict['farfieldCoeffs']['rampTime'] = '0'
    
    patchesTab = []
    if ('relaxationPatch' and 'relaxationDistance') in bvsWraper['physicsDict'] :
        
        patchesTab = getPatches(bvsWraper['physicsDict']['relaxationPatch'])
        relaxationDistanceTab = getPatches(bvsWraper['physicsDict']['relaxationDistance'])
        
        patches = ''
        farfieldDistances = ''
        blendingDistances = ''
        
        for i in range(len(patchesTab)):
            patchesTab2 = getPatches(bvsWraper['boundaryConditionsDict'][patchesTab[i]])
            for p in patchesTab2:
                patches += p + ' '
                farfieldDistances +=  relaxationDistanceTab[i] + ' '
                blendingDistances +=  str(0.9*float(relaxationDistanceTab[i])) + ' '
        
        patches = '(' + patches.strip() + ')'
        farfieldDistances = '(' + farfieldDistances.strip() + ')'
        blendingDistances = '(' + blendingDistances.strip() + ')'
        
        patchesTab = getPatches(patches)
        
        workingDict['relaxationNames'] = patches
        workingDict['farfieldCoeffs']['relaxationZone'] = collections.OrderedDict()
        workingDict['farfieldCoeffs']['relaxationZone']['relaxationScheme'] = 'farfieldList'
        workingDict['farfieldCoeffs']['relaxationZone']['farfieldPatchNames'] = patches
        workingDict['farfieldCoeffs']['relaxationZone']['farfieldDistances'] = farfieldDistances
        workingDict['farfieldCoeffs']['relaxationZone']['blendingDistances'] = blendingDistances
        
    
    workingDict['initCoeffs'] = collections.OrderedDict()
    workingDict['initCoeffs']['$farfieldCoeffs '] = ''
    
    for p in patchesTab:
        name = p.strip() + 'Coeffs'
        workingDict[name] = collections.OrderedDict()
        workingDict[name]['$farfieldCoeffs '] = ''
        
    ################### system/controlDict #########################
    workingDict = foamStarDict['system/controlDict']
    
    if 'startTime' in bvsWraper['paramDict']:
        workingDict['startTime'] = bvsWraper['paramDict']['startTime']
    if 'endTime' in bvsWraper['paramDict']:
        workingDict['endTime'] = bvsWraper['paramDict']['endTime']
    if 'timeStep' in bvsWraper['paramDict']:
        workingDict['deltaT'] = bvsWraper['paramDict']['timeStep']
    if 'saveInterval' in bvsWraper['paramDict']:
        workingDict['writeInterval'] = bvsWraper['paramDict']['saveInterval']
    if 'nbPastSaves' in bvsWraper['paramDict']:
        workingDict['purgeWrite'] = bvsWraper['paramDict']['nbPastSaves']
    
    if 'imposedVelocity' in bvsWraper['bodyDict']:
        workingDict['movingFrame']['fwdVelocity'] = bvsWraper['bodyDict']['imposedVelocity']
    if 'velocityRampTime' in bvsWraper['bodyDict']:
        workingDict['movingFrame']['rampTime'] = bvsWraper['bodyDict']['velocityRampTime']
    if 'velocityStartTime' in bvsWraper['bodyDict']:
        workingDict['movingFrame']['refTime'] = bvsWraper['bodyDict']['velocityStartTime']
        
    ################### system/decomposeParDict #########################
    workingDict = foamStarDict['system/decomposeParDict']
    
    if ('solver' in bvsWraper['paramDict']) and ('nbProcessors' in bvsWraper['paramDict']['solver']):
        workingDict['numberOfSubdomains'] = bvsWraper['paramDict']['solver']['nbProcessors']
    if  ('solver' in bvsWraper['paramDict']) and ('CPUdecompositionMethod' in bvsWraper['paramDict']['solver']):
        workingDict['method'] = bvsWraper['paramDict']['solver']['CPUdecompositionMethod']
    if  ('solver' in bvsWraper['paramDict']) and ('simpleCoeffs' in bvsWraper['paramDict']['solver']):
        workingDict['simpleCoeffs'] = bvsWraper['paramDict']['solver']['simpleCoeffs']
        
    ################### system/fvSchemes #########################
    workingDict = foamStarDict['system/fvSchemes']
    
    if ('solver' in bvsWraper['paramDict']) and ('timeSchemeOrder' in bvsWraper['paramDict']['solver']):
        if bvsWraper['paramDict']['solver']['timeSchemeOrder'] == '1':
            workingDict['ddtSchemes']['default'] = 'Euler';
        elif bvsWraper['paramDict']['solver']['timeSchemeOrder'] == '2':
            workingDict['ddtSchemes']['default'] = 'CrankNicolson 0.95';
#            workingDict['ddtSchemes']['ddt(rho,U)'] ='backward';
        else:
            print("ERROR ! Time scheme order can be only 1 or 2 ! ")
    
    ################### system/fvSolution #########################
    workingDict = foamStarDict['system/fvSolution']
    
    if ('solver' in bvsWraper['paramDict']) and ('nbPIMPLE' in bvsWraper['paramDict']['solver']):
        workingDict['PIMPLE']['nOuterCorrectors'] = bvsWraper['paramDict']['solver']['nbPIMPLE']
    if ('solver' in bvsWraper['paramDict']) and ('nbPISO' in bvsWraper['paramDict']['solver']):
        workingDict['PIMPLE']['nCorrectors'] = bvsWraper['paramDict']['solver']['nbPISO']
    if ('solver' in bvsWraper['paramDict']) and ('nbNonOrthogonalCorrectors' in bvsWraper['paramDict']['solver']):
        workingDict['PIMPLE']['nNonOrthogonalCorrectors'] = bvsWraper['paramDict']['solver']['nbNonOrthogonalCorrectors']
    if ('solver' in bvsWraper['paramDict']) and ('relaxationFactors' in bvsWraper['paramDict']['solver']):
        workingDict['relaxationFactors']['fields']['".*"'] = bvsWraper['paramDict']['solver']['relaxationFactors']
        workingDict['relaxationFactors']['equations']['".*"'] = bvsWraper['paramDict']['solver']['relaxationFactors']
    if ('solver' in bvsWraper['paramDict']) and ('PressureRelTol' in bvsWraper['paramDict']['solver']):
        workingDict['solvers']['p_rgh']['relTol'] = bvsWraper['paramDict']['solver']['PressureRelTol']
        workingDict['solvers']['p_rghFinal']['relTol'] = str(float(bvsWraper['paramDict']['solver']['PressureRelTol'])*0.1)
        
    ################### system/myOutputDict #########################
    tmpDict=importDict(refDirPath,'system','myOutputDict')

    foamStarDict['system/myOutputDict']=collections.OrderedDict()
    foamStarDict['system/myOutputDict']['#inputMode']='overwrite'
    foamStarDict['system/myOutputDict']['libs']='( "libfoamStar.so" "libsampling.so")'
    #foamStarDict['system/myOutputDict']={'#inputMode':'overwrite', 'libs':'( "libfoamStar.so" "libsampling.so")'}
    workingDict = foamStarDict['system/myOutputDict']
    
    if 'outputs' in bvsWraper['paramDict']:
        workingDict['functions']=collections.OrderedDict()
        
        if ('motions' in bvsWraper['paramDict']['outputs']) and switch(bvsWraper['paramDict']['outputs']['motions']):
            for i in range(len(bodyList)):
                name = 'motions_'+bodyList[i]
                workingDict['functions'][name]=tmpDict['functions']['motions']
    #            workingDict['functions'][name]['patches']=bodyPatches[i]
                
        if ('forces' in bvsWraper['paramDict']['outputs']) and switch(bvsWraper['paramDict']['outputs']['forces']):
            
            for i in range(min(len(bodyList),1)): #range(len(bodyList)): FIXME
#                name = 'forces_'+bodyList[i]
                name = 'forces'
                workingDict['functions'][name]=tmpDict['functions']['forces']
                workingDict['functions'][name]['patches']=bodyPatches[i]
                
                workingDict['functions'][name]['CofR']=foamStarDict['constant/dynamicMeshDict']['multiBodyFvMeshCoeffs'][bodyList[i]]['CoRInitial']
                if 'rhoWater' in bvsWraper['physicsDict']:
                     workingDict['functions'][name]['rhoInf'] = bvsWraper['physicsDict']['rhoWater']
        
        if ('freeSurface' in bvsWraper['paramDict']['outputs']) and switch(bvsWraper['paramDict']['outputs']['freeSurface']):
            workingDict['functions']['freeSurface']=tmpDict['functions']['freeSurface']
            
            if 'freeSurfaceWriteInterval' in bvsWraper['paramDict']['outputs']:
                workingDict['functions']['freeSurface']['writeInterval']=bvsWraper['paramDict']['outputs']['freeSurfaceWriteInterval']
                
        if ('bodyPatch' in bvsWraper['paramDict']['outputs']) and switch(bvsWraper['paramDict']['outputs']['bodyPatch']): # FIXME
            
            surfaceValues='(\n'
            for i in range(min(len(bodyList),1)): #range(len(bodyList)): FIXME
                name = bodyList[i]
                surfaceValues+='\t'*13+name+' {type patch; patches '+bodyPatches[i]+'; triangulate false; }\n' 
            
            surfaceValues+='\t'*12+')'
                
            workingDict['functions']['patchFields']=tmpDict['functions']['patchFields']
            
            workingDict['functions']['patchFields']['surfaces']=surfaceValues
            if 'bodyPatchWriteInterval' in bvsWraper['paramDict']['outputs']:
                workingDict['functions']['patchFields']['writeInterval']=bvsWraper['paramDict']['outputs']['bodyPatchWriteInterval']
    ################## Additionnal parameters ##################
    
    addParamPath = pth.join(bvsDirPath,'addParamDict')
    
    if pth.exists(addParamPath):
        addParamDict=importDict(bvsDirPath,'','addParamDict')
        addDict2Dict(addParamDict,foamStarDict)
        
        
    writeFolder(bvsDirPath, foamStarDict)
    
    
###################################################################################
#########                           END MAIN                              #########
###################################################################################

    
refDirPath = ''
bvsDirPath = ''

if len(sys.argv) <= 1:
    bvsDirPath = os.getcwd()
else:
    bvsDirPath = sys.argv[1]
    
if len(sys.argv) <= 2:
    refDirPath = os.environ['FOAMSTAR_REFCASE']
else:
    refDirPath = sys.argv[2]
      
print('\nREFERENCE FOLDER: ',refDirPath)
print('\nSIMULATION FOLDER: ',bvsDirPath)
print('\n\n')


main(refDirPath,bvsDirPath)



### NOTE #####

# write succesfuly two same key overwrite the first one

