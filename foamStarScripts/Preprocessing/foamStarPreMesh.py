#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 22:16:46 2021

@author: theodescamps
"""

import os
import sys
import os.path as pth
import copy
import collections

###########################################################################
######                          DICT                             ##########
###########################################################################

def writeHead(file,location,name):
    head  = "/*--------------------------------*- C++ -*----------------------------------*\ \n"
    head += "=========                 | \n"
    head += "\\\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox \n"
    head += " \\\    /   O peration     | Website:  https://openfoam.org \n"
    head += "  \\\  /    A nd           | Version:  8 \n"
    head += "   \\\/     M anipulation  | \n"
    head += "\*---------------------------------------------------------------------------*/ \n"
#    head += "FoamFile \n"
#    head += "{ \n"
#    head += "\tversion     2.0; \n"
#    head += "\tformat      ascii; \n"
#    head += "\tclass       dictionary; \n"
#    head += '\tlocation    "'+location+'"; \n'
#    head += '\tobject      '+name+'; \n'
#    head += "} \n"
#    head += "// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * // \n\n\n"
    
    file.write(head)
    
def cleanComment(buff):
    
    sl=[]
    el=[]
    
    skip = 0
    comment = False
    n = len(buff)
    k=0
    while k < n-1:
        
        if comment:
            if buff[k]=='\n':
                if skip == 0:
                    el.append(k)
                comment = False
        else:
            if buff[k:k+2]=='//':
                if skip == 0:
                    sl.append(k)
                comment = True
                k+=1
                
            elif buff[k:k+2]=='/*':
                if skip == 0:
                    sl.append(k)
                skip += 1
                k+=1
                
            elif buff[k:k+2]=='*/':
                skip -= 1
                if skip == 0:
                    el.append(k+2)
                if skip < 0:
                    print ("ERROR ! Unexpected '*/' was found")
                    return
                k+=1
        k+=1
        
    
    if len(sl) == 0:
        return buff
    
    else:
        buffc = buff[:sl[0]]
        for i in range(len(sl)-1):
            buffc+=buff[el[i]:sl[i+1]]
        
        if len(el) == len(sl):
            buffc+=buff[el[-1]:]
        
    return buffc

def cleanVal(val):
    valc = ""
    last = False
    for c in val:
        if c =='\n' or c =='\t' or c ==' ':
            if not last:
                last=True
                valc+=' '
        else:
            last=False
            valc+=c
    return valc

def readDict(buff, dico, path):
    indict = 0

    n=len(buff)
    
    k, sl =0, 0
    
    key = ""
    val = ""
    actDict = [dico]
    waitKey = True
    inKey = False
    
    bracket = 0
    
    while k<n:
        
        if buff[k] == '(':
            bracket+=1
            if waitKey:
                print ("ERROR ! Unexpected '(' was found. Waiting for Key")
                return
            
        elif buff[k] == ')':
            bracket-=1
            if waitKey:
                print ("ERROR ! Unexpected ')' was found. Waiting for Key")
                return
            
            if bracket < 0:
                print ("ERROR ! Unexpected ')' was found")
                return
            
        elif bracket == 0:
            if buff[k] == '{':
                if inKey:
                    key = buff[sl:k].strip()
                    sl=k
                    inKey = False
                    
                waitKey = True
                
                if len(key) == 0:
                    print ("ERROR ! Unamed subDict")
                    return
                
                indict+=1
                actDict[-1][key]=collections.OrderedDict()
                actDict.append(actDict[-1][key])
                
                sl=k+1
                
            elif buff[k] == '}':
                indict-=1
                if indict < 0:
                    print ("ERROR ! Unexpected '}' was found")
                    return
                
                actDict.pop()
                waitKey = True
                key =""
                sl=k+1
            
            elif (buff[k] == " " or buff[k] == "\t" or buff[k] == "\n") and inKey:
                key = buff[sl:k].strip()
                sl=k+1
                inKey = False
                actDict[-1][key]=''
                if len(key) == 0:
                    print ("ERROR ! Empty key was found")
                    return
                elif key[0]=='$':
                    waitKey = True
                    key = ""
                
            elif buff[k] == ';' or (buff[k] == "\n" and key == '#include'): #Write key; as {key:""}
                if inKey:
                    key = buff[sl:k].strip()
                    sl=k
                    inKey = False
                
                if len(key) != 0:
                    val = cleanVal(buff[sl:k].strip())
                    actDict[-1][key]=val
                waitKey = True
                key = ""
            
            elif waitKey and buff[k] != " " and buff[k] != "\t" and buff[k] != "\n":
                waitKey = False
                inKey = True
                sl=k
            
        k+=1
    if bracket != 0:
        print ("ERROR ! '( )' syntax error in file: ", path)
        return
    if indict != 0:
        print ("ERROR ! '{ }' syntax error in file: ", path)
        return
        
def importDict(dirPath,location,name):
    path = pth.join(dirPath,location)
    path = pth.join(path,name)
    
    f = open(path,'r')
    if not f:
        print ("ERROR oppenning file: ", path)
        return
    
    buff = cleanComment(f.read())
    dico = collections.OrderedDict()
    readDict(buff, dico, path)
    return  dico


def writeDict(dirPath,location,name, dico):
    path = pth.join(dirPath,location)
    path = pth.join(path,name)
    
    file = open(path,'w')
    if not file:
        print ("ERROR oppenning file: ", path)
        return
    
    
    lenMaxKey = 40
    
    writeHead(file,location,name)

    actDict=[dico]
    actPos=[0]
    
    nbTab = 0 
    nb=0
    nbMax = 1000
    dictKey = ""
    while len(actDict) > 0 and nb < nbMax :
        while actPos[-1] < len(actDict[-1])  and nb < nbMax :
            nb+=1
            keyList=[]
            for i in actDict[-1].keys():
                keyList.append(i)
                
            key = keyList[actPos[-1]]
            value = actDict[-1][key]
            
            if type(value) is collections.OrderedDict:
                dictKey = key
                actPos[-1]+=1
                actDict.append(value)
                actPos.append(0)
                line = '\n'+'\t'*nbTab + key + '\n' + '\t'*nbTab + '{\n'
                file.write(line)
                nbTab+=1
                
                
            elif len(key) > 0 and key[0]=='$':
                line = '\t'*nbTab + key + '\n'
                
                file.write(line)
                actPos[-1]+=1
            elif len(value)>0:
                
                line = '\t'*nbTab + key + ' '*(lenMaxKey-len(key)) + value + ';\n'
                
                if key == "#include":
                    line = '\t'*nbTab + key + ' '*(lenMaxKey-len(key)) + value + '\n'
                
                file.write(line)
                actPos[-1]+=1
            else:
                line = '\t'*nbTab + key + ';\n'
                
                file.write(line)
                actPos[-1]+=1
                
        nbTab-=1
        
            
        actPos.pop()
        actDict.pop()
        
        if len(actDict) > 0:
            if dictKey == "FoamFile":
                line ='\t'*nbTab + '}\n'
                line += "// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * // \n\n\n"
            else:
                line ='\t'*nbTab + '}\n\n'
            file.write(line)
        
    if nb==nbMax:
        print('ERROR ! Maximal number of entry reached !')
    
def addDict2Dict(source, target):
    
    actTaget = [target]
    actDict = [source]
    actPos=[0]
    path =['']
    
    nb=0
    nbMax = 1000
    
    while len(actDict) > 0 and nb < nbMax :
        while actPos[-1] < len(actDict[-1])  and nb < nbMax :
            nb+=1
            keyList=[]
            for i in actDict[-1].keys():
                keyList.append(i)
                
            key = keyList[actPos[-1]]
            value = actDict[-1][key]
            
            if type(value) is collections.OrderedDict:
                
                if not(key in actTaget[-1]):
                    actTaget[-1][key]=collections.OrderedDict()
                    print ('INFO: New entry "',key,'{} "  was added to the dictionary "',path[-1],'" !')
                            
                actPos[-1]+=1
                actDict.append(value)
                actPos.append(0)
                if len(path) > 1:
                    path.append(path[-1]+'->'+key)
                else:
                    path.append(path[-1]+key)
                actTaget.append(actTaget[-1][key])
                
                
            else:
                if not(key in actTaget[-1]):
                    print ('INFO: New entry "',key,' = ',value,'" was added to the dictionary "',path[-1],'" !')
                else:
                    print ('INFO: The entry "',key,' = ',actTaget[-1][key],'" was modified by "',key,' = ',value,'" in the dictionary "',path[-1],'" !')
                
                actTaget[-1][key] = value
                actPos[-1]+=1

        
        actTaget.pop()
        actPos.pop()
        actDict.pop()
        path.pop()
        
    if nb==nbMax:
        print('ERROR ! Maximal number of entry reached !')
    
    
    
        
def writeFolder(dirPath, folderDict):
    
    for key, value in folderDict.items():
        path = pth.join(dirPath,key)
        
        if not pth.exists(pth.dirname(path)):
            cmd = "mkdir -p " + pth.dirname(path)
            os.system(cmd)
        
        writeDict(dirPath, pth.dirname(key), pth.basename(key), value)
        

def getV(dico,key,default):
    if ('key' in dico):
        return dico[key]
    else:
        return default


def addPatches(init, toAdd):
    initStrip = init.strip()
    addStrip = toAdd.strip()
    tab1 = []
    tab2 = []
    if len(initStrip) > 0:
        tab1=initStrip[1:-1].split()
    if len(addStrip) > 0:
        tab2=addStrip[1:-1].split()
    final="("
    for x in tab1:
        final += x + ' '
    for x in tab2:
        if not(x in tab1):
            final += x + ' '
    final+=")"
        
    return final

def getPatches(patches):
    patchesStrip = patches.strip()
    tab = []
    if len(patchesStrip) > 0:
        tab=patchesStrip[1:-1].split()
    return tab

def getVect(vect):
    vectStrip = vect.strip()
    tab = []
    if len(vectStrip) > 0:
        if vectStrip[0]=='(':
            tab=vectStrip[1:-1].split()
        else:
            tab=vectStrip.split()
            if len(tab)>1:
                print('ERROR! More than one value are defined in "',vectStrip,'". Please use () to define vectors.')
                return
    for i in range(len(tab)):
        tab[i]=float(tab[i])
    return tab

def getVectI(vect):
    vectStrip = vect.strip()
    tab = []
    if len(vectStrip) > 0:
        if vectStrip[0]=='(':
            tab=vectStrip[1:-1].split()
        else:
            tab=vectStrip.split()
            if len(tab)>1:
                print('ERROR! More than one value are defined in "',vectStrip,'". Please use () to define vectors.')
                return
    for i in range(len(tab)):
        tab[i]=int(tab[i])
    return tab

def nbPatches(patches):
    patchesStrip = patches.strip()
    tab = []
    if len(patchesStrip) > 0:
        tab=patchesStrip[1:-1].split()
    return len(tab)

def patch2regEx(patches):
    patchtab = getPatches(patches)
    
    if len(patchtab) > 1:
        key = '"('
        for p in patchtab[:-1]:
            key+=p+'|'
        
        key+=patchtab[-1]
        key += ')"'
        return key
    elif len(patchtab) > 0:
        return patchtab[0]
    else:
        return ''

def switch(p):
    return (p.lower() == 'true' or p.lower() == 'yes')



###########################################################################
######                        END DICT                           ##########
###########################################################################
    






def getCSsize(name):
    path = "./constant/polyMesh/sets/"+name
    
    file = open(path, "r")
    
    if not(file):
        print("ERROR Openning file: ", path)
        exit()

    line=""
    last=""
    lstr=""
    while line and len(lstr)==0 or lstr!='(':
        
        line=file.readline()
        if len(lstr)>0:
            last = lstr
        lstr = line.strip()
    
    if not(line):
        print("ERROR reading file: ", path)
        exit()
    
    tab = last.split()
    
    return int(tab[0])


def extractPointsSourceInfo(fileOut,edge): #WARNING structure nbpoint /n ( /n ..... /n )
    
    pathIn = "./constant/triSurface/"+edge
    fileIn = open(pathIn,'r')
    
    if not(fileIn):
        print ("ERROR oppenning: ",pathIn)
        exit()
    
    line=""
    lstr=""
    
    while len(lstr)==0 or lstr[0]!='(':
        line=fileIn.readline()
        lstr=line.strip()
    
    line=fileIn.readline()
    
    fileOut.write("sourceInfo{ points\n(")
    while len(lstr)==0 or lstr[0]!=')':
        
        fileOut.write(line)
        
        line=fileIn.readline()
        lstr=line.strip()
        
    
    fileOut.write(");}}\n")
    
    return
    
def hole(l):
    for i in l:
        for j in i:
            if j == 0:
                return True
    return False
        
def somme(l,ref):
    v = 0
    for i in l:
        for j in ref:
            v+=i[j]
    return v

def sommeV(l,c):
    v = 0
    for i in l:
        v+=i[c]
    return v

def NewBox(file, setName, box):
    action = "\t{name ptmp; type pointSet; action new; source boxToPoint; sourceInfo {box ("
    action += str(box[0])+' '+str(box[1])+' '+str(box[2])+') ('
    action += str(box[3])+' '+str(box[4])+' '+str(box[5])+');}}\n'
    action += "\t{name "+setName+"; type cellSet; action new; source boxToCell; sourceInfo {box ("
    action += str(box[0])+' '+str(box[1])+' '+str(box[2])+') ('
    action += str(box[3])+' '+str(box[4])+' '+str(box[5])+');}}\n'
    action += "\t{name "+setName+"; type cellSet; action add; source pointToCell; sourceInfo {set ptmp; option any;}}\n"
    
    file.write(action)
    
def NewRegion(file, setName, rData, rType, rTypeDict):
    
    rToPoint = rType+'ToPoint'
    rToCell = rType+'ToCell'
    
    sourceInfo='sourceInfo {'
    dataIndex=0
    for entry, dim in rTypeDict[rType].items():
        #FIXME ugly exeption:
        if entry == 'cornerMin':
            sourceInfo +='box ('
            sourceInfo += str(rData[dataIndex+0])+' '+str(rData[dataIndex+1])+' '+str(rData[dataIndex+2])+') ('
            sourceInfo += str(rData[dataIndex+3])+' '+str(rData[dataIndex+4])+' '+str(rData[dataIndex+5])+'); '
            dataIndex+=6
            
        elif entry != 'cornerMax':
            if dim > 1:
                sourceInfo+=entry+' ('
                for i in range(dim):
                    sourceInfo+=str(rData[dataIndex])+' '
                    dataIndex+=1
                sourceInfo+='); '
            else:
                sourceInfo+=entry+' '+str(rData[dataIndex])+'; '
                dataIndex+=1
            
    sourceInfo+='}'
    
    
    action = "\t{name "+setName+"; type cellSet; action new; source "+rToCell+"; "+sourceInfo+"}\n"
    
    if rType == 'box':
        action += "\t{name ptmp; type pointSet; action new; source "+rToPoint+"; "+sourceInfo+"}\n"
        action += "\t{name "+setName+"; type cellSet; action add; source pointToCell; sourceInfo {set ptmp; option any;}}\n"
    file.write(action)


def extend(file, setName, direction, size=2):
    
    faceName=setName+'f'
    pointName=setName+'p'
    
    
    action = ""
    
    
    if direction!='XYZ':
        
        action = "\t{name "+faceName+"; type faceSet; action new; source cellToFace; sourceInfo {set "+setName+"; option all;}}\n"
    
        normal = ""
        normal_= ""
    
        if direction=='X':
            
            normal="(1 0 0)"
            normal_="(-1 0 0)"
            
        elif direction=='Y':
            
            normal="(0 1 0)"
            normal_="(0 -1 0)"
            
        elif direction=='Z':
            
            normal="(0 0 1)"
            normal_="(0 0 -1)"
    
        else:
            print ("ERROR !")
            return
        action += "\t{name "+faceName+"; type faceSet; action delete; source cellToFace; sourceInfo{set "+setName+"; option both;}}\n"
        action += "\t{name ftmp; type faceSet; action new; source faceToFace; sourceInfo{set "+faceName+"; option all;}}\n"
        
        action += "\t{name "+faceName+"; type faceSet; action subset; source normalToFace; sourceInfo{normal "+normal+"; cos 0.01;}}\n"
        action += "\t{name "+setName+"; type cellSet; action add; source faceToCell; sourceInfo {set "+faceName+"; option any;}}\n"
        
        action += "\t{name ftmp; type faceSet; action subset; source normalToFace; sourceInfo{normal "+normal_+"; cos 0.01;}}\n"
        action += "\t{name "+setName+"; type cellSet; action add; source faceToCell; sourceInfo{set ftmp; option any;}}\n"
        
        file.write(action)
    else:
        
        for i in range(size):
#            action = "\t{name "+faceName+"; type faceSet; action new; source cellToFace; sourceInfo {set "+setName+"; option all;}}\n"
#            action += "\t{name "+setName+"; type cellSet; action new; source faceToCell; sourceInfo {set "+faceName+"; option any;}}\n"
            
            action = "\t{name "+pointName+"; type pointSet; action new; source cellToPoint; sourceInfo {set "+setName+"; option all;}}\n"
            action += "\t{name "+setName+"; type cellSet; action new; source pointToCell; sourceInfo {set "+pointName+"; option any;}}\n"
            file.write(action)
    



#def ST(a,b):
#    return max(a,b)

def ST(a,b):
    return a+b


def fullExtend(file, setName,nX,nY,nZ,size=2):
    
    coefX = 1
    coefY = 1
    coefZ = 1
    
#    if nX>0:
#        coefX = 0.5
#    if nY>0:
#        coefY = 0.5
#    if nZ>0:
#        coefZ = 0.5
    
    if nX<ST(nY,nZ):
        for j in range(int(((ST(nY,nZ)-nX-1)*coefX+1.)*size)):
            extend(file, setName, 'X')
    elif nY<ST(nX,nZ):
        for j in range(int(((ST(nX,nZ)-nY-1)*coefY+1.)*size)):
            extend(file, setName, 'Y')
    elif nZ<ST(nX,nY):
        for j in range(int(((ST(nX,nY)-nZ-1)*coefZ+1.)*size)):
            extend(file, setName, 'Z')
            
#    if nX<max(nY,nZ):
#        for j in range(int((nX+nY+nZ-1)*coefX)):
#            extend(file, setName, 'X',size)
#    elif nY<max(nX,nZ):
#        for j in range(int((max(nX,nZ)-nX-1)*coefY)):
#            extend(file, setName, 'Y',size)
#    elif nZ<max(nX,nY):
#        for j in range(int((max(nX,nY)-nX-1)*coefZ)):
#            extend(file, setName, 'Z',size)
            
    return
    


def limitSplitSur(file, setName,surface,lim,act,baseSize):
    
#    near=baseSize/(2**act)*2
    near = 0
    if act > lim:
        action = "\t{name "+setName+"; type cellSet; action delete; source surfaceToCell; "
        action+= 'sourceInfo {file "./constant/triSurface/'+surface+'"; outsidePoints ((-1 0.1 0.1)); includeCut true; includeInside false; includeOutside false; '
        action+= "nearDistance "+str(near)+"; curvature -100;}}\n"
        
        file.write(action)
        
def refineSurface(file, setName,surface,act,BS):
    
    near=BS/(2**act)*4
    action = "\t{name "+setName+"; type cellSet; action new; source surfaceToCell; "
    action+= 'sourceInfo {file "./constant/triSurface/'+surface+'"; outsidePoints ((-1 0.1 0.1)); includeCut true; includeInside false; includeOutside false; '
    action+= "nearDistance "+str(near)+"; curvature -100;}}\n"
    
    file.write(action)
    
def refineEdge(file, setName,edge,act,BS):
    
#    near=BS/(2**act)*2
    action = "\t{name "+setName+"; type cellSet; action new; source nearestToCell; "
    file.write(action)
    extractPointsSourceInfo(file,edge)
    
    extend(file, setName, 'XYZ') 

def intersectBorder(file, setName0, setName1, Ri, ri):
    
    Ci = ['X','Y','Z']
    faceName=setName1+'f'
    
    action = "\t{name "+faceName+"; type faceSet; action new; source cellToFace; sourceInfo {set "+setName1+"; option all;}}\n"
    action += "\t{name "+faceName+"; type faceSet; action delete; source cellToFace; sourceInfo{set "+setName1+"; option both;}}\n"
    action += "\t{name "+setName1+"; type cellSet; action subset; source faceToCell; sourceInfo {set "+faceName+"; option any;}}\n"
    action += "\t{name "+setName1+"; type cellSet; action subset; source cellToCell; sourceInfo{set "+setName0+"; option all;}}\n"
    
    if Ri[(ri+1)%3]:
        extend(file, setName1, Ci[(ri+2)%3])
    if Ri[(ri+2)%3]:
        extend(file, setName1, Ci[(ri+1)%3])
    
    action += "\t{name "+setName1+"; type cellSet; action subset; source cellToCell; sourceInfo{set "+setName0+"; option all;}}\n"
    action += "\t{name "+setName0+"; type cellSet; action add; source cellToCell; sourceInfo{set "+setName1+"; option all;}}\n"
    file.write(action)

def writeBlockMesh(dirPath,meshDict):
    refDirPath = os.environ['FOAMSTAR_REFCASE']
    
    refDict=collections.OrderedDict()
    
    if (meshDict["domain"]["type"] == "3DFullDomain"):
        refDict=importDict(refDirPath,'system/preMesh','3DFullDomain_blockMeshDict')
    elif(meshDict["domain"]["type"] == "3DHalfDomain"):
       refDict=importDict(refDirPath,'system/preMesh','3DHalfDomain_blockMeshDict')
    elif(meshDict["domain"]["type"] == "2DDomain"):
        refDict=importDict(refDirPath,'system/preMesh','2DDomain_blockMeshDict')
    else:
        print("ERROR ! Domain/type have to be: 3DFullDomain or 3DHalfDomain or 2DDomain")
        return
    
    cornerMin = getPatches(meshDict["domain"]["cornerMin"])
    cornerMax = getPatches(meshDict["domain"]["cornerMax"])
    
    refDict["xMin"]=cornerMin[0]
    refDict["yMin"]=cornerMin[1]
    refDict["zMin"]=cornerMin[2]
    
    refDict["xMax"]=cornerMax[0]
    refDict["yMax"]=cornerMax[1]
    refDict["zMax"]=cornerMax[2]
    
    refDict["divX"]=str( max( 1, int( ( float(cornerMax[0])-float(cornerMin[0]) ) / float(meshDict["domain"]["baseSize"]) + 0.5 ) ) )
    refDict["divY"]=str( max( 1, int( ( float(cornerMax[1])-float(cornerMin[1]) ) / float(meshDict["domain"]["baseSize"]) + 0.5 ) ) )
    refDict["divZ"]=str( max( 1, int( ( float(cornerMax[2])-float(cornerMin[2]) ) / float(meshDict["domain"]["baseSize"]) + 0.5 ) ) )
    
    
    
    writeDict(dirPath, 'system', 'blockMeshDict', refDict)
    
    
    
    
def main(refDirPath,dirPath):
    
    topoPath = "./system/topoSetDict"
    
    head  = "/*--------------------------------*- C++ -*----------------------------------*\ \n"
    head += "=========                 | \n"
    head += "\\\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox \n"
    head += " \\\    /   O peration     | Website:  https://openfoam.org \n"
    head += "  \\\  /    A nd           | Version:  8 \n"
    head += "   \\\/     M anipulation  | \n"
    head += "\*---------------------------------------------------------------------------*/ \n"
    head += "FoamFile \n"
    head += "{ \n"
    head += "\tversion     2.0; \n"
    head += "\tformat      ascii; \n"
    head += "\tclass       dictionary; \n"
    head += '\tlocation    "system"; \n'
    head += '\tobject      topoSetDict; \n'
    head += "} \n"
    head += "// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * // \n\n\n"
    
    
    meshDict=importDict(dirPath,'.','meshDict')
    writeBlockMesh(dirPath,meshDict)
    os.system("rm -rf constant/polyMesh")
    os.system("blockMesh")
    
    
    refDirPath = os.environ['FOAMSTAR_REFCASE']
    refineDictPath = pth.join(pth.join(refDirPath,'system/preMesh'),'refineMeshXDict')
    refineDictPathTarget = pth.join(dirPath,'system')
    cmd = "cp -fv "+ refineDictPath + " " + refineDictPathTarget
    os.system(cmd)
    
    refDirPath = os.environ['FOAMSTAR_REFCASE']
    refineDictPath = pth.join(pth.join(refDirPath,'system/preMesh'),'refineMeshYDict')
    refineDictPathTarget = pth.join(dirPath,'system')
    cmd = "cp -fv "+ refineDictPath + " " + refineDictPathTarget
    os.system(cmd)
    
    refDirPath = os.environ['FOAMSTAR_REFCASE']
    refineDictPath = pth.join(pth.join(refDirPath,'system/preMesh'),'refineMeshZDict')
    refineDictPathTarget = pth.join(dirPath,'system')
    cmd = "cp -fv "+ refineDictPath + " " + refineDictPathTarget
    os.system(cmd)
    
    refDirPath = os.environ['FOAMSTAR_REFCASE']
    refineDictPath = pth.join(pth.join(refDirPath,'system/preMesh'),'refineMeshXYZDict')
    refineDictPathTarget = pth.join(dirPath,'system')
    cmd = "cp -fv "+ refineDictPath + " " + refineDictPathTarget
    os.system(cmd)
    
    
    
    BS = float(meshDict['domain']['baseSize'])
    
    
    surfaces = []
    Rs = []
    
    edges = []
    Re = []
    
    regions = []
    R = []
    rType = []
    
    rTypeDict = collections.OrderedDict()
    rTypeDict["box"]=collections.OrderedDict([("cornerMin",3),("cornerMax",3)])
    rTypeDict["cylinder"]=collections.OrderedDict([("p1",3),("p2",3),("radius",1)])
    rTypeDict["cylinderAnnulus"]=collections.OrderedDict([("p1",3),("p2",3),("innerRadius",1),("outerRadius",1)])
    
    
    
    
    if 'surfaces' in meshDict:
        for key,value in meshDict['surfaces'].items():
            surfaces.append(value['fileName'])
            Rs.append(getVectI(value['refinementXYZ']))
    
    if 'edges' in meshDict:
        for key,value in meshDict['edges'].items():
            edges.append(value['fileName'])
            Re.append(getVectI(value['refinementXYZ']))
            
    if 'regions' in meshDict:
        for key,value in meshDict['regions'].items():
            rType.append(value['type'])
            if not(rType[-1] in rTypeDict):
                print('ERROR! regions type "',rType[-1],'" is not accepted by this script !')
                return
            
            rData=[]
            for entry, dim in rTypeDict[rType[-1]].items():
                rData+=getVect(value[entry])
            regions.append(rData)
                
            R.append(getVectI(value['refinementXYZ']))
            
    Robj = copy.deepcopy(Rs)

    Ract=[0,0,0]
            
    N = len(R)
    Ns = len(Rs)
    Ne = len(Re)
    
#    while not(hole(R) or hole(Rs) or hole(Re)): #FIXME
#    while False:   
#        file = open(topoPath,'w')
#        file.write(head)
#        file.write("\n\nactions\n(\n")
#        
#        action = "\t{name c0; type cellSet; action new; source boxToCell; sourceInfo {box (0 0 0) (0 0 0);}}\n"
#        file.write(action)
#        
#        
#        for i in range(N):
#            NewBox(file, "c1", boxes[i])
#            
#            fullExtend(file, "c1",R[i][0],R[i][1],R[i][2])
#            
#            action = "\t{name c0; type cellSet; action add; source cellToCell; sourceInfo{set c1; option all;}}\n"
#            
#            file.write(action)
#            
#            R[i][0]-=1
#            R[i][1]-=1
#            R[i][2]-=1
#            
#        for i in range(Ns):
#
#            refineSurface(file, 'c1',surfaces[i],Ract[0],BS)
#            fullExtend(file, "c1",Rs[i][0],Rs[i][1],Rs[i][2])
#            
#            action = "\t{name c0; type cellSet; action add; source cellToCell; sourceInfo{set c1; option all;}}\n"
#            
#            file.write(action)
#            
#            Rs[i][0]-=1
#            Rs[i][1]-=1
#            Rs[i][2]-=1
#            
#        for i in range(Ne):
#
#            refineEdge(file, 'c1',edges[i],Ract[0],BS)
#            fullExtend(file, "c1",Re[i][0],Re[i][1],Re[i][2])
#            
#            action = "\t{name c0; type cellSet; action add; source cellToCell; sourceInfo{set c1; option all;}}\n"
#            
#            file.write(action)
#            
#            Re[i][0]-=1
#            Re[i][1]-=1
#            Re[i][2]-=1
#            
#        Ract[0]+=1
#        Ract[1]+=1
#        Ract[2]+=1
#            
#        extend(file, "c0", 'XYZ') 
#        
#        file.write("\n);\n")
#        file.close()
#        
#        os.system("topoSet")
#        os.system("refineMesh -dict system/refineMeshXYZDict -overwrite")
#        exit()
        
        
    
    Ci = ['X','Y','Z']
    refinementOrder=[2,1,0]
    if meshDict['domain']['type'] == '2DDomain':
        refinementOrder=[2,0]
        
        
    refinementDict=['refineMeshXDict','refineMeshYDict','refineMeshZDict']
    incr = 0
    stop = 20
    while somme(R,refinementOrder)+somme(Rs,refinementOrder)+somme(Re,refinementOrder) != 0 and incr != stop:
        justRef=[]
        justRefs=[]
        justRefe=[]
        incr += 1
        for ri in refinementOrder:
            if sommeV(R,ri)+sommeV(Rs,ri)+sommeV(Re,ri)!= 0:
                
                file = open(topoPath,'w')
                file.write(head)
        
                file.write("\n\nactions\n(\n")
                action = "\t{name c0; type cellSet; action new; source boxToCell; sourceInfo {box (0 0 0) (0 0 0);}}\n"
                file.write(action)
                
                for i in range(N):
                    
                    if R[i][ri] != 0:
#                        NewBox(file, "c1", regions[i])
                        NewRegion(file, "c1", regions[i], rType[i], rTypeDict)
                        fullExtend(file, "c1",R[i][0],R[i][1],R[i][2])
                                
                        
                        action = "\t{name c0; type cellSet; action add; source cellToCell; sourceInfo{set c1; option all;}}\n"
                        file.write(action)
                        
                        R[i][ri]-=1
                        justRef.append(i)
                        
                for i in range(Ns):

                    if Rs[i][ri] != 0:
                        refineSurface(file, 'c1',surfaces[i],Robj[i][ri],BS)
                        fullExtend(file, "c1",Rs[i][0],Rs[i][1],Rs[i][2])
                                
                        
                        action = "\t{name c0; type cellSet; action add; source cellToCell; sourceInfo{set c1; option all;}}\n"
                        file.write(action)
                        
                        Rs[i][ri]-=1
                        justRef.append(i)
                        
                for i in range(Ne):

                    if Re[i][ri] != 0:
                        refineEdge(file, 'c1',edges[i],Ract[ri],BS)
                        fullExtend(file, "c1",Re[i][0],Re[i][1],Re[i][2])
                                
                        
                        action = "\t{name c0; type cellSet; action add; source cellToCell; sourceInfo{set c1; option all;}}\n"
                        file.write(action)
                        
                        Re[i][ri]-=1
                        justRef.append(i)
                        
            
                Ract[ri]+=1   
                extend(file, "c0", 'XYZ') 
                
                file.write("\n);\n")
                file.close()
            
                os.system("topoSet")
                
                
                
                nbCellsLast = 0
                nbCells = getCSsize("c0")
                secure = 20
                iterations = 0
                
                
                
                
#                while nbCells != nbCellsLast and iterations < secure:
#                    iterations +=1
#                    
#                    file = open(topoPath,'w')
#                    file.write(head)
#                    file.write("\n\nactions\n(\n")
#                    action = "\t{name c2; type cellSet; action new; source boxToCell; sourceInfo {box (0 0 0) (0 0 0);}}\n"
#                    file.write(action)
#                
#                    for i in range(N):
#                        if not(i in justRef) and R[i][ri] == 0 and sommeV(R,ri) != 0:
#                            NewBox(file, "c1", boxes[i])
#                            fullExtend(file, "c1",R[i][0],R[i][1],R[i][2])
#                            intersectBorder(file,"c0","c1",R[i],ri)
#
#                    for i in range(Ns):
#                        if not(i in justRefs) and Rs[i][ri] == 0  and sommeV(Rs,ri) != 0:
#                            refineSurface(file, 'c1',surfaces[i],Ract[ri],BS)
#                            fullExtend(file, "c1",Rs[i][0],Rs[i][1],Rs[i][2])
#                            intersectBorder(file,"c0","c1",Rs[i],ri)
#                            
#                    for i in range(Ne) :
#                        if not(i in justRefe) and Re[i][ri] == 0 and sommeV(Re,ri) != 0:
#                            refineEdge(file, 'c1',edges[i],Ract[ri],BS)
#                            fullExtend(file, "c1",Re[i][0],Re[i][1],Re[i][2])
#                            intersectBorder(file,"c0","c1",Re[i],ri)
#                    
#                    file.write("\n);\n")
#                    file.close()
#                    os.system("topoSet")
#                    nbCellsLast = nbCells
#                    nbCells = getCSsize("c0")
                    
                    
                if iterations == secure:
                    print("ERROR max iter reached !")
                    exit()
                        
                cmd = "refineMesh -dict system/"+refinementDict[ri]+" -overwrite"
                os.system(cmd)
        
    print("INCR: ",incr)
    
#def snappyPreset(refDirPath,bvsDirPath):
#    refDict=collections.OrderedDict()
#    refDict["system/meshQualityDict"]=importDict(refDirPath,"system","meshQualityDict")
#    refDict["system/snappyHexMeshDict"]=importDict(refDirPath,"system","snappyHexMeshDict")
#    refDict["system/surfaceFeaturesDict"]=importDict(refDirPath,"system","surfaceFeaturesDict")
    

REF_DIR_PATH = ''
BVS_DIR_PATH = ''

if len(sys.argv) <= 1:
    BVS_DIR_PATH = os.getcwd()
else:
    BVS_DIR_PATH = sys.argv[1]
    
if len(sys.argv) <= 2:
    REF_DIR_PATH = os.environ['FOAMSTAR_REFCASE']
else:
    REF_DIR_PATH = sys.argv[2]
      
print('\nREFERENCE FOLDER: ',REF_DIR_PATH)
print('\nSIMULATION FOLDER: ',BVS_DIR_PATH)
print('\n')

os.chdir(BVS_DIR_PATH)
os.system("cp -rv $FOAM_INST_DIR/foamStarDoc/ReferenceCases/SnappySetup/system .")
main(REF_DIR_PATH,'.')


# blockMesh+refineMesh -> constant/polyMesh
# snappy -> step 1 (cf control dict)
# mv constant/polyMesh -> MeshSteps/blockMesh
# mv 0/constant/polymesh -> constant/polymesh

# Check str in dict